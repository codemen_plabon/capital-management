/**
 * Created by Plabon Ghosh on 25/08/2015.
 */


var AppDispatcher = require('../dispatcher/AppDispatcher');
var AppConstants = require('../constants/AppConstants');

var CommonAction = {


    SetAppKey: function (appKey) {

        AppDispatcher.handleViewAction({
            actionType: AppConstants.ADD_APP_KEY,
            data: appKey
        });
    },

    SetKey: function (key) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.ADD_KEY,
            data: key
        });
    },

    SetUserName: function (userName) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.ADD_USER_NAME,
            data: userName
        });
    },

    SetUserFulName: function (userFullName) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.ADD_USER_FULL_NAME,
            data: userFullName
        });
    },

    SetUserType: function (userType) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.ADD_USER_TYPE,
            data: userType
        });
    },

    SetUserPermission: function (userPermission) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.ADD_USER_PERMISSION,
            data: userPermission
        });
    },

    SetUserId: function (userId) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.ADD_USER_ID,
            data: userId
        });
    },


    SetUserEmail: function (userEmail) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.ADD_USER_EMAIL,
            data: userEmail
        });
    },


    setApiUrl: function (premiseUrl, nosqlUrl) {
        AppDispatcher.handleViewAction({
            actionType: AppConstants.SET_API_URL,
            data: {premiseUrl: premiseUrl, nosqlUrl: nosqlUrl}
        });
    },

    setSearchCredentials: function (data) {
        AppDispatcher.handleViewAction({
            actionType: "SET_SEARCH_CREDENTIALS",
            data: data
        });
    },

    setUserInfo: function (data) {
        AppDispatcher.handleViewAction({
            actionType: "SET_USER_INFO",
            data: data
        });
    }
};

module.exports = CommonAction;
