/**
 * Created by Shishir on 28/03/2016.
 */

var AppDispatcher = require('../dispatcher/AppDispatcher');
var AppConstants = require('../constants/AppConstants');

var TopMenuAtion = {

    AddMenuFromServer: function(data){
        AppDispatcher.handleServerAction({
            actionType: AppConstants.ADD_MENU_ITEMS_FROM_SERVER,
            data:data
        });
    },

    AddPermittedApp:function(appList){
        AppDispatcher.handleViewAction({
            actionType: AppConstants.ADD_PERMITTED_APP,
            data: appList
        });
    },

};

module.exports = TopMenuAtion;