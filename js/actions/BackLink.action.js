/**
 * Created by Plabon Ghosh on 25/08/2015.
 */


var AppDispatcher = require('../dispatcher/AppDispatcher');
var AppConstants = require('../constants/AppConstants');

var BackLinkActions = {
    AddItem: function(item){
        AppDispatcher.handleViewAction({
            actionType: AppConstants.ADD_BACK_LINK,
            data: item
        });
    },

    RemoveItem: function(item){
        AppDispatcher.handleViewAction({
            actionType: AppConstants.REMOVE_BACK_LINK,
            data: item
        });
    }
};

module.exports = BackLinkActions;
