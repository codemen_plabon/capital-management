/**
 * Created by Plabon Ghosh on 25/08/2015.
 */


var AppDispatcher = require('../dispatcher/AppDispatcher');
var AppConstants = require('../constants/AppConstants');

var CurrentViewAction = {
    addItem: function(viewName,editId,otherInfo){
        AppDispatcher.handleViewAction({
            actionType: AppConstants.SET_CURRENT_VIEW,
            data: {viewName:viewName,editId:editId}
        });
    }
};

module.exports = CurrentViewAction;
