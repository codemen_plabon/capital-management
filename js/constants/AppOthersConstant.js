/**
 * Created by plabo on 18/10/2015.
 */

/*

 Dev_API Endpoints :
 1. http://testapptudio.apptudio.com
 2. http://testpremise.apptudio.com

 TEST_API (for Clients Only) :
 1. http://nosql.apptudio.com
 2. http://premise.apptudio.com

                        DEV                                                             TEST
  http://testapptudio.apptudio.com            ==>>               http://nosql.apptudio.com
  http://testpremise.apptudio.com            ==>>               http://premise.apptudio.com

*/

var CommonStore = require('../stores/Common.store');

var AppOthersConstant = {
    API_MAIN_DOMAIN: "",
    API_PREMISE_DOMAIN:"",

    setUrl:function(api,premise){
        this.API_MAIN_DOMAIN = api;
        this.API_PREMISE_DOMAIN = premise;
    }
};


// var AppOthersConstant = {
//     API_MAIN_DOMAIN: "http://nosql.premisehq.co",
//     API_PREMISE_DOMAIN:"http://www2.premisehq.co",
//     DEFAULT_TASK_MODULE_ID:"e6c6fc03b2b04dc9adffbccd4b5e185a_Default_task",
//     DEFAULT_TASK_ID:"94d02bb53975483fb464956c4b2b3d4e"
// };

// var AppOthersConstant = {
//     API_MAIN_DOMAIN: "http://stage-nosql.premisehq.co",
//     API_PREMISE_DOMAIN:"http://stage.premisehq.co",
//     DEFAULT_TASK_MODULE_ID:"e6c6fc03b2b04dc9adffbccd4b5e185a_Default_task",
//     DEFAULT_TASK_ID:"94d02bb53975483fb464956c4b2b3d4e"
// };

module.exports = AppOthersConstant;
