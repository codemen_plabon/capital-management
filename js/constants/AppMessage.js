/**

 * Created by plabo on 18/10/2015.

 */

var AppMessage = {

        FORM_SAVE:"Form request save successfully.",
        COMMON_ERROR_MESSAGE:"Something went worng. Please try again later.",
        LOAD_USER_INFO:'Getting user information...',
        LOAD_APP_PERMISSION:'Getting permitted apps...',
        LOAD_Employee_LIST: "Getting Users...",

        FORM_DATA_DELETE: "Form Data Delete Request Successful...",
        PASSWORD_VALIDATION: "password and confirmPassword are not matching... ",
        REQUIRD: " fill all the required field ... "

};

module.exports = AppMessage;