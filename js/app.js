
var React = require('react');
var ReactDOM = require('react-dom');
var CurrentView = require('./components/CurrentView.react');
require('babel-polyfill');


var CommonAction = require('./actions/Common.action');
var CommonApi = require('./utils/ServerApi/CommonApi');
//var IndexPage = require('./Index');
var Navigator = require('./components/Navigator.react.js');
var WindowFunciton = require('../js/utils/WindowFunction');
function getParameterByNameIndex(name) {
    name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
    var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"),
        results = regex.exec(location.search);
    return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
}

function readTextFile(file, callback) {
    var rawFile = new XMLHttpRequest();
    rawFile.overrideMimeType("application/json");
    rawFile.open("GET", file, true);
    rawFile.onreadystatechange = function() {
        if (rawFile.readyState === 4 && rawFile.status == "200") {
            callback(rawFile.responseText);
        }
    }
    rawFile.send(null);
}

function getCookie(cname) {
    var name = cname + "=";
    var decodedCookie = decodeURIComponent(document.cookie);
    var ca = decodedCookie.split(';');
    for(var i = 0; i <ca.length; i++) {
        var c = ca[i];
        while (c.charAt(0) == ' ') {
            c = c.substring(1);
        }
        if (c.indexOf(name) == 0) {
            return c.substring(name.length, c.length);
        }
    }
    return "";
}

var externalUser = $("<link>", {
    "rel": "stylesheet",
    "type": "text/css",
    "href": "content/css/profileStyle.css"
})[0];




    var url=document.location.origin;
    url="https://demo.premisehq.co"
    var key = getParameterByNameIndex('Key');
    var AppKey = getParameterByNameIndex('AppKey');

    CommonAction.setSearchCredentials({
        Key: key,
        AppKey: AppKey
    });

    if(key==null || key =="")
    {
        try {
            var data = getCookie("Token");
            var tempData = JSON.parse(data);
            key = tempData[0]["Key"];
            key = key.replace("@", "=");
            key = key.replace("@", "=");
        }catch (ex){
            window.location.href = "401.html";
        }
       // alert(key);
    }

    if(AppKey==null || AppKey==""){
        var data = getCookie("Token");
        var tempData = JSON.parse(data);
        AppKey = tempData[0]["Appkey"];

    }

    //CommonAction.SetAppKey(getParameterByNameIndex('AppKey'));
    //CommonAction.SetKey(getParameterByNameIndex('Key'));
    CommonAction.SetAppKey(AppKey);
    CommonAction.SetKey(key);
    CommonAction.SetUserName(getParameterByNameIndex('UserName'));
    CommonAction.SetUserType(getParameterByNameIndex('UserType'));
    CommonAction.SetUserPermission(getParameterByNameIndex('Permission'));
    CommonAction.SetUserId(getParameterByNameIndex('UserId'));

    readTextFile(url+"/APIDomains/Protocols.json", function(text){
        var data = JSON.parse(text);
        var apiUrl="";
        var premiseUrl="";

        if(data.HTTPS.IsActive=="true")
        {
            apiUrl=data.HTTPS.NoSQL;
            premiseUrl=data.HTTPS.Premise;
        }
        else {
            apiUrl = data.HTTP.NoSQL
            premiseUrl = data.HTTP.Premise;
        }
        
        CommonAction.setApiUrl(premiseUrl,apiUrl);
    CommonApi.getUserInfo(function (userInfo) {
        CommonAction.setUserInfo(userInfo);
        CommonApi.getMenuItems(function () {
            CommonApi.getPermittedApp(function () {
                window.dispatchEvent(new Event('popstate'));
            });
        });
    });
    });

    /*ReactDOM.render(
        <IndexPage/>,
        document.getElementById('Top-Area')
    );*/

    ReactDOM.render(<Navigator />, document.querySelector('#Top-Area'));

    ReactDOM.render(
        <CurrentView />,
        document.getElementById('main-content')
    );











