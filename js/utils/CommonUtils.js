/**
 * Created by Shishir on 10/05/2016.
 */

var moment = require('moment');

var CommonUtils = {
    ToLocalDate:function(instanceDate,dateTimeFormat,flag){
        /*
        var utcDate=moment(instanceDate).format('YYYY-MM-DD HH:mm:ss');
        var localTime = moment(utcDate).local().format('MM/DD/YYYY HH:mm:ss');
        return localTime;
        */
        var localTime  = moment.utc(instanceDate,"YYYY-MM-DD HH:mm").toDate();
        localTime = moment(localTime).format('MM/DD/YYYY');
        return localTime;
    },

    ToLocalDate:function(utcDate,dateFormat){
        //utcDate = new Date(utcDate);
        localTime = moment(utcDate).format(dateFormat);
        return localTime;
    },

    ToUTCDate:function(localDate,dateFormat){
        return moment(localDate).format('MM/DD/YYYY HH:mm:ss');
    },

    IsNullOrUndefined:function(value){
        if(value == null || value == '' || value == undefined){
            return true;
        }

    },

    GetBuildingId:function(PropBldg){
        var BldgId = '';
        if(!this.IsNullOrUndefined(PropBldg) && !this.IsNullOrUndefined(PropBldg.value)){
            BldgId = PropBldg.value.split('-')[1];
        }
        else {
            BldgId = '';
        }

        return BldgId;
    },

    getSearchPairs: function () {
        var keyValuePair = {};

        var getParams = location.search.substr(1);
        getParams.split('&').map(function (item) {
            var splitterIndex = item.indexOf('=');
            var key = item.substr(0, splitterIndex);
            if (key) {
                keyValuePair[key] = item.substr(splitterIndex + 1);
            }
        });
        return keyValuePair;
    },

    getSearchStr: function (keyValuePair) {
        var str = "";
        for (var key in keyValuePair) {
            str += "&" + key + "=" + keyValuePair[key];
        }
        return str.substr(1);
    }
}

module.exports = CommonUtils;