/**
 * Created by Dipon on 18/07/2016.
 */
var $ = require('jquery');
var CommonStore = require('../../stores/Common.store');
var AppOthersConstant = require('../../constants/AppOthersConstant');
var AppMessage = require('../../constants/AppMessage');
var SnackBar = require('node-snackbar');
var CommonAction = require('../../actions/Common.action');

var RoleApi = {

    saveRole: function(roleData, callback){

        showLoader("","Please Wait.." ,0);
        var url = "";
        var objthis = this;
        if(roleData.Did == null || roleData.Did == '' ){
            url = AppOthersConstant.API_MAIN_DOMAIN + "/api/" + CommonStore.getAppKey() + "/ModuleData/Premise_Role?key=" + CommonStore.getKey();
        }else{
            url = AppOthersConstant.API_MAIN_DOMAIN + "/api/" + CommonStore.getAppKey() + "/ModuleData/UpdateData/Premise_Role/" + roleData.Did + "?key="+ CommonStore.getKey();
        }

        var formData = new FormData();
        formData.append('RoleName', roleData.RoleName);
        formData.append('Description', roleData.Description);
        formData.append('AppId', "");
        $.ajax({
            async: true,
            type: 'POST',
            url: url,
            data: formData,
            contentType: false,
            processData: false
        }).done(function (data){
            objthis.saveRoleReference(data,roleData,callback)

        }).fail(function (data){
            if (data.status == 401)
                window.location.href = "401.html";
            else
                hideLoader();
        });
    },
    saveRoleReference:function(roleparentData,roleData,callback){
        var url = "";

        if(roleData.Did == null || roleData.Did == '' ){
            //url = AppOthersConstant.API_MAIN_DOMAIN + "/api/" + CommonStore.getAppKey() + "/ModuleData/UserRole?key=" + CommonStore.getKey();
            url=AppOthersConstant.API_MAIN_DOMAIN + "/API/"+ CommonStore.getAppKey() +"/APIRoles/SaveRole?Key="+ CommonStore.getKey();
        }else{
            //url = AppOthersConstant.API_MAIN_DOMAIN + "/api/Roles/SaveRole?"+CommonStore.getAppKey();
            url=AppOthersConstant.API_MAIN_DOMAIN + "/API/"+ CommonStore.getAppKey() +"/APIRoles/UpdateRole?Key="+ CommonStore.getKey() + "&Did=" + roleData.Did;

        }

        var formData = new FormData();
        formData.append('Did',roleparentData.Data);
        formData.append('RoleName', roleData.RoleName);
        formData.append('RoleDescription', roleData.Description);


        $.ajax({
            async: true,
            type: 'POST',
            url: url,
            data: formData,
            contentType: false,
            processData: false
        }).done(function (data){
            hideLoader();
            if(callback){
                callback(roleparentData);
            }
        }).fail(function (data){
            if (data.status == 401)
                window.location.href = "401.html";
            else
                hideLoader();
        });
    },

    getRole: function(id,callBack){
        showLoader("","Please Wait.." ,0);
        var url = "";

        if(id==null) {
            url = AppOthersConstant.API_MAIN_DOMAIN + "/api/" + CommonStore.getAppKey() + "/ModuleData/Premise_Role/?key=" + CommonStore.getKey();
        }
        else{
            url = AppOthersConstant.API_MAIN_DOMAIN + "/api/" + CommonStore.getAppKey() + "/ModuleData/Premise_Role/" + id + "?key=" + CommonStore.getKey();
        }
        $.ajax({
            async: true,
            type: 'GET',
            url: url
        }).done(function (data) {
            hideLoader();
            // CommonAction.setAppStateLoading(false);
            var formatData = JSON.parse(data.Data);
            var dataArray = new Array();
            $.each(formatData, function(index, formatData){
                var data = {
                    Did: formatData.Did,
                    RoleName: formatData.RoleName,
                    Description: formatData.Description,
                    AppId: formatData.AppId
                }
                dataArray.push(data);
            });

            if(callBack)
                callBack(dataArray);

        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
            else
                hideLoader();
        });
    },

    deleteRole: function(id,callBack){
        CommonAction.setAppStateLoading(true, "Please wait...");

        var url=AppOthersConstant.API_MAIN_DOMAIN+"/api/"+CommonStore.getAppKey()+"/ModuleData/DeleteData/Premise_Role/"+id+"?key="+CommonStore.getKey();

        $.ajax({
            async: true,
            type: 'POST',
            url: url
        }).done(function (data){
            CommonAction.setAppStateLoading(false);
            SnackBar.show({text: AppMessage.FORM_DATA_DELETE, showActionButton: false});
        }).fail(function (data){
            if (data.status == 401)
                window.location.href = "401.html";
            else
                hideLoader();
        });
    }

};

module.exports = RoleApi;