/**
 * Created by Dipon on 18/07/2016.
 */
var $ = require('jquery');
var CommonStore = require('../../stores/Common.store');
var AppOthersConstant = require('../../constants/AppOthersConstant');
var AppMessage = require('../../constants/AppMessage');
var SnackBar = require('node-snackbar');
var CommonAction = require('../../actions/Common.action');

var BudgetApi = {

    getBudget: function(callBack){
        showLoader("","Please Wait.." ,0);
        var url = AppOthersConstant.API_MAIN_DOMAIN + "/api/" + CommonStore.getAppKey() + "/ModuleData/CEM_V1_Budget/?key=" + CommonStore.getKey();

        $.ajax({
            async: true,
            type: 'GET',
            url: url
        }).done(function (data) {
           // hideLoader();
            var formatData = JSON.parse(data.Data);
            if(callBack)
                callBack(formatData);

        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
            else
                hideLoader();
        });
    },

    getBudgetByID: function(Id,callBack){
       // showLoader("","Please Wait.." ,0);
        var url =AppOthersConstant.API_PREMISE_DOMAIN+ "/api/CapitalExpence/GetBudget?AppKey="+CommonStore.getAppKey()+"&Key="+CommonStore.getKey()+"&BudgetID="+Id;
        $.ajax({
            async: true,
            type: 'GET',
            url: url
        }).done(function (data) {
            hideLoader();
            if(callBack)
                callBack(data);

        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
            else
                hideLoader();
        });
    },

    UpdateBudget:function(data,callBack)
    {
        showLoader("","Please Wait.." ,0);
        var formData=new FormData();
        formData.append("from",data.from);
        formData.append("ColumnName",data.header);
        formData.append("Data",data.changeData);
        formData.append("BudgetID",data.budgetId);
        formData.append("BudgetNo",data.budgetNumber);
        formData.append("BudgetTitle",data.budgetTitle);
        formData.append("PropertyId",data.propertyId);
        formData.append("PropertyName",data.propertyName);
        var url =AppOthersConstant.API_PREMISE_DOMAIN+ "/api/CapitalExpence/UpdateBudget?AppKey="+CommonStore.getAppKey()+"&Key="+CommonStore.getKey();
        $.ajax({
            async: true,
            type: 'POST',
            url: url,
            data: formData,
            contentType: false,
            processData: false
        }).done(function (data){
          hideLoader();
            if(callBack)
                callBack(data);

        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
            else
                hideLoader();
        });
    },

    getApproveBudgetProject: function(skip,limit,callBack){
        // showLoader("","Please Wait.." ,0);
        var url =AppOthersConstant.API_PREMISE_DOMAIN+ "/api/CapitalExpence/GetApproveBudgetProjectList?AppKey="+CommonStore.getAppKey()+"&Key="+CommonStore.getKey()+"&Limit="+limit+"&Skip="+skip;
        $.ajax({
            async: true,
            type: 'GET',
            url: url
        }).done(function (data) {
            hideLoader();
            if(callBack)
                callBack(data);

        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
            else
                hideLoader();
        });
    },

};

module.exports = BudgetApi;