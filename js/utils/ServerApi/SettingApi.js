/**
 * Created by plabo on 12/07/2017.
 */


var $ = require('jquery');
var CommonStore = require('../../stores/Common.store');
var AppOthersConstant = require('../../constants/AppOthersConstant');
var SettingApi = {

    getApprovalSettingForm: function (id, callBack) {

        showLoader("", "Please Wait..", 0);

        var url = "";
        if (id == null || id == "") {
            url = AppOthersConstant.API_MAIN_DOMAIN + "/FE/FormEngine/Forms?AppKey=" + CommonStore.getAppKey() + "&Key=" + CommonStore.getKey() + "&Page=" + "99f90c6dc2c746f8ad8742e9ae7a077b";
        } else {
            url = AppOthersConstant.API_MAIN_DOMAIN + "/FE/FormEngine/Forms?AppKey=" + CommonStore.getAppKey() + "&Key=" + CommonStore.getKey() + "&Page=" + "99f90c6dc2c746f8ad8742e9ae7a077b" + "&DID=" + id;
        }

        $.ajax({
            async: true,
            type: 'GET',
            url: url
        }).done(function (data) {
            hideLoader();
            if (callBack)
                callBack(data);
        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
            else {
                hideLoader();
            }

        });
    },
    getProjectTypeSettingForm: function (id, callBack) {

        showLoader("", "Please Wait..", 0);

        var url = "";
        if (id == null || id == "") {
            url = AppOthersConstant.API_MAIN_DOMAIN + "/FE/FormEngine/Forms?AppKey=" + CommonStore.getAppKey() + "&Key=" + CommonStore.getKey() + "&Page=" + "48905f81e7664e3282906d0e7db1d9f6";
        } else {
            url = AppOthersConstant.API_MAIN_DOMAIN + "/FE/FormEngine/Forms?AppKey=" + CommonStore.getAppKey() + "&Key=" + CommonStore.getKey() + "&Page=" + "48905f81e7664e3282906d0e7db1d9f6" + "&DID=" + id;
        }

        $.ajax({
            async: true,
            type: 'GET',
            url: url
        }).done(function (data) {
            hideLoader();
            if (callBack)
                callBack(data);
        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
            else {
                hideLoader();
            }

        });
    },
    getProjectWiseYearlyBudgetSetting: function (id, callBack) {

        showLoader("", "Please Wait..", 0);

        var url = "";
        if (id == null || id == "") {
            url = AppOthersConstant.API_MAIN_DOMAIN + "/FE/FormEngine/Forms?AppKey=" + CommonStore.getAppKey() + "&Key=" + CommonStore.getKey() + "&Page=" + "286bc405976c48989f8c1f6c90a13d4d";
        } else {
            url = AppOthersConstant.API_MAIN_DOMAIN + "/FE/FormEngine/Forms?AppKey=" + CommonStore.getAppKey() + "&Key=" + CommonStore.getKey() + "&Page=" + "286bc405976c48989f8c1f6c90a13d4d" + "&DID=" + id;
        }

        $.ajax({
            async: true,
            type: 'GET',
            url: url
        }).done(function (data) {
            hideLoader();
            if (callBack)
                callBack(data);
        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
            else {
                hideLoader();
            }

        });
    },

    getProjectForm: function (id, callBack) {

        showLoader("", "Please Wait..", 0);

        var url = "";
        if (id == null || id == "") {
            url = AppOthersConstant.API_MAIN_DOMAIN + "/FE/FormEngine/Forms?AppKey=" + CommonStore.getAppKey() + "&Key=" + CommonStore.getKey() + "&Page=" + "8999c6711ed84f2f95393c7fdca65efd";
        } else {
            url = AppOthersConstant.API_MAIN_DOMAIN + "/FE/FormEngine/Forms?AppKey=" + CommonStore.getAppKey() + "&Key=" + CommonStore.getKey() + "&Page=" + "8999c6711ed84f2f95393c7fdca65efd" + "&DID=" + id;
        }

        $.ajax({
            async: true,
            type: 'GET',
            url: url
        }).done(function (data) {
            hideLoader();
            if (callBack)
                callBack(data);
        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
            else {
                hideLoader();
            }

        });
    }
}


module.exports = SettingApi;