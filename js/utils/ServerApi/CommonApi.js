/**
 * Created by Plabon on 14/01/2016.
 */
var CommonAction = require('../../actions/Common.action');
var $ = require('jquery');
var CommonStore=require('../../stores/Common.store');
var AppOthersConstant = require('../../constants/AppOthersConstant');
var AppMessage = require('../../constants/AppMessage');
var _ =require('underscore');

var TopMenuAction = require('../../actions/TopMenu.action');
var CommonApi= {

    getPermittedApp: function (callback) {

        showLoader("", AppMessage.LOAD_APP_PERMISSION, 0);

        var url = "";
        var obj = this;

        var appKey = "";
        var key = "";

        url = AppOthersConstant.API_MAIN_DOMAIN + "/api/APIUserPermission/getPermittedApp?key=" + CommonStore.getKey() + "&AppKey=" + CommonStore.getAppKey() + "&UserType=" + CommonStore.getUserType();
        $.ajax({
            async: true,
            type: 'GET',
            url: url
        }).done(function (data) {
            hideLoader();
            if (data.Status == "Success") {
                TopMenuAction.AddPermittedApp(data.Data);

                if (callback) {
                    callback();
                }
            }

        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
            else
                hideLoader();
        });
    },

    getUserInfo: function (callBack) {
        showLoader("", AppMessage.LOAD_USER_INFO, 0);
        var url = "";
        var obj = this;

        var appKey = "";
        var key = "";

        url = AppOthersConstant.API_MAIN_DOMAIN + "/api/APIUserPermission/getUserInfo?key=" + CommonStore.getKey() + "&AppKey=" + CommonStore.getAppKey() + "&UserInfo=1&&IsGetPermission=true&AppId=CEM";

        $.ajax({
            async: true,
            type: 'GET',
            url: url
        }).done(function (data) {
            hideLoader();
            if (data.Status == "Success") {
                var data = data.Data;
                CommonAction.SetUserName(data.UserName);
                CommonAction.SetUserFulName(data.FirstName + ' ' + data.LastName);
                CommonAction.SetUserType(data.UserType);
                CommonAction.SetUserPermission(data.Permission);
                CommonAction.SetUserId(data.UserId);
                if (callBack) {
                    callBack(data);
                }
            }

        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
            else
                hideLoader();
        });
    },

    /*   Get Menu ITEMS  */
    getMenuItems: function (callBack) {
        showLoader("", "Getting menu items", 0);


        var url = AppOthersConstant.API_PREMISE_DOMAIN + "/API/PremisePermission/Getmenu?key="
            + CommonStore.getKey() + "&UserId=" + CommonStore.getUserId()
            + "&AppKey=" + CommonStore.getAppKey() + "&AppId=CEM";


        $.ajax({
            type: 'GET',
            url: url,
            async: true
        }).done(function (data) {
            //var MenuJSON = JSON.parse(data);
            hideLoader();
            TopMenuAction.AddMenuFromServer(data);

            if (callBack) {
                callBack();
            }
        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
            else
                hideLoader();
        });
    },

    getEmployee: function (callBack) {
        showLoader("", AppMessage.LOAD_Employee_LIST, 0);

        var url = AppOthersConstant.API_PREMISE_DOMAIN + "/API/NewPM/GetEmployee?AppKey=" + CommonStore.getAppKey() + "&Key=" + CommonStore.getKey();
        var option = [];
        $.ajax({
            async: true,
            type: 'GET',
            url: url
        }).done(function (data) {
            hideLoader();
            var formatData = JSON.parse(data);
            CommonAction.addEmployee(formatData);
            $.each(formatData, function (index, formatData) {

                data = {
                    value: formatData.Id,
                    label: formatData.Name
                };
                option.push(data);
            });


            if (callBack) {
                callBack(option);
            }

        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
            else
                hideLoader();
        });

    },

    getFormUserMappingData: function (formId, callBack) {
        showLoader("", "Getting form user list.", 0);

        var url = AppOthersConstant.API_MAIN_DOMAIN + "/api/" + CommonStore.getAppKey() + "/ModuleData/FE_FormsUserMapping/SqlQuery/?key=" + CommonStore.getKey() + "&query={\"query\":[{ \"Where\": \"$and:[{ModuleKey},{FormId:'" + formId + "'} ]\"}]}";
        var option = [];
        $.ajax({
            async: true,
            type: 'GET',
            url: url
        }).done(function (data) {
            hideLoader();
            var formatData = JSON.parse(data.Data);
            CommonAction.addEmployee(formatData);
            $.each(formatData, function (index, formatData) {

                data = {
                    FormId: formatData.FormId,
                    UserId: formatData.UserId,
                    UserName: formatData.UserName
                };
                option.push(data);
            });


            if (callBack) {
                callBack(option);
            }

        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
            else
                hideLoader();
        });

    },


    getBuilding: function (callBack) {

        showLoader("", "Loading Property/Building", 0);

        var url = "";
        var obj = this;
        url = AppOthersConstant.API_MAIN_DOMAIN + "/api/" + CommonStore.getAppKey() + "/ModuleData/Inventory_Building/SqlQuery/?key=" + CommonStore.getKey() + "&query={\"query\":[{ \"Where\": \"$and:[{ModuleKey},{Active:'1'} ]\"}]}";

        //url = AppOthersConstant.API_MAIN_DOMAIN + "/api/" + CommonStore.getAppKey() + "/ModuleData/Inventory_Building?key=" + CommonStore.getKey();

        /*if (process.env.NODE_ENV != 'production') {
            url = "./temp-storage/getBuilding.json";
        }*/

        $.ajax({
            async: true,
            type: 'GET',
            url: url
        }).done(function (data) {
            if (data.Status == "Success") {
                hideLoader();
                if (callBack)
                    callBack(data.Data);
            }

        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
            else
                hideLoader();
        });
    },

    getProperty: function (callback) {
        showLoader("", "Loading Property/Building", 0);

        var url = "";
        var obj = this;

        url = AppOthersConstant.API_MAIN_DOMAIN + "/api/" + CommonStore.getAppKey() + "/ModuleData/Inventory_Property/SqlQuery/?key=" + CommonStore.getKey() + "&query={\"query\":[{ \"Where\": \"$and:[{ModuleKey},{IsActive:'1'} ]\"}]}";

        /*if (process.env.NODE_ENV != 'production') {
            url = "./temp-storage/getProperty.json";
        }*/

       // url = AppOthersConstant.API_MAIN_DOMAIN + "/api/" + CommonStore.getAppKey() + "/ModuleData/Inventory_Property?key=" + CommonStore.getKey();

        $.ajax({
            async: true,
            type: 'GET',
            url: url
        }).done(function (data) {

            if (data.Status == "Success") {

                hideLoader();
                if (callback)
                    callback(data.Data);
            }

        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
            else
                hideLoader();
        });

    },

    getFloor: function (BuildingId, callBack) {

        showLoader("", "Loading Floor", 0);
        var url = "";
        var obj = this;

        url = AppOthersConstant.API_MAIN_DOMAIN + "/api/" + CommonStore.getAppKey() + "/ModuleData/Inventory_Floor/SqlQuery/?key=" + CommonStore.getKey() + "&query={\"query\":[{ \"Where\": \"$and:[{ModuleKey},{BuildingId:'" + BuildingId + "'} ]\"}]}";

        $.ajax({
            async: true,
            type: 'GET',
            url: url
        }).done(function (data) {
            hideLoader();
            if (data.Status == "Success") {
                if (callBack)
                    callBack(data.Data);
            }
        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
            else
                hideLoader();

        });

    },

    getSuite: function (FloorId, callBack) {
        showLoader("", "Loading Suit", 0);

        var url = "";
        var obj = this;

        url = AppOthersConstant.API_MAIN_DOMAIN + "/api/" + CommonStore.getAppKey() + "/ModuleData/Inventory_Suite/SqlQuery/?key=" + CommonStore.getKey() + "&query={\"query\":[{ \"Where\": \"$and:[{ModuleKey},{FloorId:'" + FloorId + "'} ]\"}]}";


        $.ajax({
            async: true,
            type: 'GET',
            url: url
        }).done(function (data) {

            if (data.Status == "Success") {
                hideLoader();

                if (callBack)
                    callBack(data.Data);
            }
        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
            else
                hideLoader();
        });

    },


 getSuiteByBuildingId: function (buildingId, callBack) {
        showLoader("", "Loading Suit", 0);

        var url = "";
        var obj = this;

        url = AppOthersConstant.API_MAIN_DOMAIN + "/api/" + CommonStore.getAppKey() + "/ModuleData/Inventory_Suite/SqlQuery/?key=" + CommonStore.getKey() + "&query={\"query\":[{ \"Where\": \"$and:[{ModuleKey},{BuildingId:'" + buildingId + "'} ]\"}]}";


        $.ajax({
            async: true,
            type: 'GET',
            url: url
        }).done(function (data) {

            if (data.Status == "Success") {
                hideLoader();

                if (callBack)
                    callBack(data.Data);
            }
        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
            else
                hideLoader();
        });

    }, 
    
    getTotalDataCount: function (moduleName,condition, callBack) {

        showLoader("", "Please wait..", 0);
        var formData = new FormData();
        formData.append("ModuleKey", moduleName);
        formData.append("Condition", condition);
        formData.append("Distinct", "");
        formData.append("Count", "true");
        var url = AppOthersConstant.API_MAIN_DOMAIN + "/" + "api/" + CommonStore.getAppKey() + "/ModuleData/Aggregate?key=" + CommonStore.getKey();

        $.ajax({
            async: true,
            data:formData,
            type: 'POST',
            url: url,
            contentType: false,
            processData: false
        }).done(function (data) {

            if (data.Status == "Success") {
                hideLoader();
                if (callBack)
                    callBack(data.Data);
            }
        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
            else
                hideLoader();
        });
    },

    GetDataFromAggregate: function (moduleName,condition, callBack) {

        showLoader("", "Please wait..", 0);
        var formData = new FormData();
        formData.append("ModuleKey", moduleName);
        formData.append("Condition", condition);
        formData.append("Distinct", "");
        formData.append("Count", "");
        var url = AppOthersConstant.API_MAIN_DOMAIN + "/" + "api/" + CommonStore.getAppKey() + "/ModuleData/Aggregate?key=" + CommonStore.getKey();

        $.ajax({
            async: true,
            data:formData,
            type: 'POST',
            url: url,
            contentType: false,
            processData: false
        }).done(function (data) {

            if (data.Status == "Success") {
                hideLoader();
                var formatData = JSON.parse(data.Data);
                if (callBack)
                    callBack(formatData);
            }
        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
            else
                hideLoader();
        });
    }
}


module.exports=CommonApi;