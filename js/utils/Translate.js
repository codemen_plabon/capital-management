/**
 * Created by plabon on 07/11/2016.
 */



var content = require('../../language.json');
var CommonStore = require('../stores/Common.store');
var Transaction = {
    getContent:function(language) {

        if (language == '' || language == undefined)
            language = 'en';

        for(var i=0;i<content.length;i++) {

            if(content[i].lang==language)
                return content[i];
        }
        return "";
    },

    convertText:function(key)    {
       var tramslateData =   this.getContent(CommonStore.getLanguage()).data;
        return tramslateData[key];
    }


};

module.exports = Transaction;