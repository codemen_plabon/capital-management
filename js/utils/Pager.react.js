/**
 * Created by Plabon Ghosh on 04/02/2016.
 */

var React = require('react');

function range(e, a) {
    for (var t = [], s = e; a > s; s++) t.push(s);
    return t
}
var BASE_SHIFT = 0,
    TITLE_SHIFT = 1,
    Pager = React.createClass({
        displayName: "Pager",
        propTypes: {
            current: React.PropTypes.number.isRequired,
            total: React.PropTypes.number.isRequired,
            visiblePages: React.PropTypes.number.isRequired,
            onPageChanged: React.PropTypes.func,
            onPageSizeChanged: React.PropTypes.func
        },
        handleFirstPage: function() {
            this.isPrevDisabled() || this.handlePageChanged(BASE_SHIFT)
        },
        handlePreviousPage: function() {
            this.isPrevDisabled() || this.handlePageChanged(this.props.current - TITLE_SHIFT)
        },
        handleNextPage: function() {
            this.isNextDisabled() || this.handlePageChanged(this.props.current + TITLE_SHIFT)
        },
        handleLastPage: function() {
            this.isNextDisabled() || this.handlePageChanged(this.props.total - TITLE_SHIFT)
        },
        handleMorePrevPages: function() {
            var e = this.calcBlocks();
            this.handlePageChanged(e.current * e.size - TITLE_SHIFT)
        },
        handleMoreNextPages: function() {
            var e = this.calcBlocks();
            this.handlePageChanged((e.current + TITLE_SHIFT) * e.size)
        },
        handlePageChanged: function(e) {
            var a = this.props.onPageChanged;
            a && a(e)
        },
        calcBlocks: function() {
            var e = this.props,
                a = e.total,
                t = e.visiblePages,
                s = e.current + TITLE_SHIFT,
                i = Math.ceil(a / t),
                n = Math.ceil(s / t) - TITLE_SHIFT;
            return {
                total: i,
                current: n,
                size: t
            }
        },
        isPrevDisabled: function() {
            return this.props.current <= BASE_SHIFT
        },
        isNextDisabled: function() {
            return this.props.current >= this.props.total - TITLE_SHIFT
        },
        isPrevMoreHidden: function() {
            var e = this.calcBlocks();
            return e.total === TITLE_SHIFT || e.current === BASE_SHIFT
        },
        isNextMoreHidden: function() {
            var e = this.calcBlocks();
            return e.total === TITLE_SHIFT || e.current === e.total - TITLE_SHIFT
        },
        visibleRange: function() {
            var e = this.calcBlocks(),
                a = e.current * e.size,
                t = this.props.total - a,
                s = a + (t > e.size ? e.size : t);
            return [a + TITLE_SHIFT, s + TITLE_SHIFT]
        },
        render: function() {
            return React.createElement("nav", null, React.createElement("ul", {
                className: "pagination"
            }, React.createElement(Page, {
                className: "btn-first-page",
                isDisabled: this.isPrevDisabled(),
                onClick: this.handleFirstPage
            }, "First"), React.createElement(Page, {
                className: "btn-prev-page",
                isDisabled: this.isPrevDisabled(),
                onClick: this.handlePreviousPage
            }, "«"), React.createElement(Page, {
                isHidden: this.isPrevMoreHidden(),
                onClick: this.handleMorePrevPages
            }, "..."), this.renderPages(this.visibleRange()), React.createElement(Page, {
                isHidden: this.isNextMoreHidden(),
                onClick: this.handleMoreNextPages
            }, "..."), React.createElement(Page, {
                className: "btn-next-page",
                isDisabled: this.isNextDisabled(),
                onClick: this.handleNextPage
            }, "»"), React.createElement(Page, {
                className: "btn-last-page",
                isDisabled: this.isNextDisabled(),
                onClick: this.handleLastPage
            }, "Last")))
        },
        renderPages: function(e) {
            var a = this;
            return range(e[0], e[1]).map(function(e, t) {
                var s = e - TITLE_SHIFT,
                    i = a.handlePageChanged.bind(null, s),
                    n = a.props.current === s;
                return React.createElement(Page, {
                    key: t,
                    isActive: n,
                    className: "btn-numbered-page",
                    onClick: i
                }, e)
            })
        }
    }),
    Page = React.createClass({
        displayName: "Page",
        render: function() {
            if (this.props.isHidden) return null;
            var e = (this.props.className + " " || "") + (this.props.isActive ? "active" : "") + (this.props.isDisabled ? " disabled" : "");
            return React.createElement("li", {
                key: this.props.key,
                className: e
            }, React.createElement("a", {
                onClick: this.props.onClick
            }, this.props.children))
        }
    });

module.exports = Pager;
