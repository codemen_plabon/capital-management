/**
 * Created by Suza on 7/21/2016.
 */

var CurrentViewAction = require('../actions/CurrentView.action');
var CurrentViewStore = require('../stores/CurrentView.store');
var CommonStore = require('../stores/Common.store');
var AppOthersConstant = require('../constants/AppOthersConstant');
var AppMessage = require('../constants/AppMessage');
var CommonApi = require('./ServerApi/CommonApi');
var moment = require('moment');
var _ = require('underscore');
var $ = require('jquery');
var SnackBar = require('node-snackbar');
var CommonAction = require('../actions/Common.action');
var CommonUtils = require('./CommonUtils');
var getSearchPairs = CommonUtils.getSearchPairs;

window.loadCallBack = function () {

    if (CurrentViewStore.getCurrentView() == "PurchaseOrder") {
        $("#ProjectID").on("change", function () {
            $("#ProjectName").val($(this).find('option:selected').text());
        });
    }

    if (CurrentViewStore.getCurrentView() == "ApprovalSettings") {

        setTimeout(function () {
            BindRoleInProjectTypeSettings();
        }, 1000);

    }

    if (CurrentViewStore.getCurrentView() == "ProjectTypeSetting") {
        $("#PropertyID").on("change", function () {
            $("#PropertyName").val($(this).find('option:selected').text());

            $("[id$='DPropertyID']").each(function(index, item){
                $(item).val($("#PropertyID").val());
            });

            $("[id$='DPropertyName']").each(function(index, item){
                $(item).val($("#PropertyName").val());
            });
        });

        $("#ASTMStandardID").on("change", function () {
            $("#ASTMStandardName").val($(this).find('option:selected').text());
        });

        setTimeout(function () {
            BindRoleInProjectTypeSettings();
        }, 1000);
    }

    if (CurrentViewStore.getCurrentView() == "PropertyWiseYearlyBudget") {

        $("#PropertyID").on("change", function () {
            $("#PropertyName").val($(this).find('option:selected').text());
        });
    }
    if (CurrentViewStore.getCurrentView() == "NewProject") {

        $("#Div_76").css("display", "none");
        var readOnly=getSearchPairs().readOnly;
        if(readOnly=="true") {
            $("#Div_76").css("display", "block");
            readOnlyProject();
        }
        ProcessProjectBreakDown(true);


        $("#Div_56").css("display", "none");
        $("#Div_2").css("display", "none");
        $("#Div_8").css("display", "none");
        $("#CheckBoxContainer_CEAType input").each(function(index, item){
            $(item).click(function(){
                if($(this).val() == "Budgeted"){
                    //$("#Div_56").css("display", "block");
                }
                else {
                    $("#Div_56").css("display", "none");
                }
            });
        });

        if(CurrentViewStore.getCurrentEditId() == ""){
            $("#BudgetApprovalStatus").val("Approved");
        }

        $("#PropertyID").on("change", function () {
            $("#PropertyName").val($(this).find('option:selected').text());
        });

        $("#txtProjNo").on("change", function () {
            $("#ProjNo").val($(".lblProjYear").text() + $(this).val());
        });

        setTimeout(function () {
            GetTotal();
        }, 1000);

        setTimeout(function () {
            ProcessProjectBreakDown(false);
        }, 1000);
    }


}

function readOnlyProject(){
    setTimeout(function () {
        $("input").each(function(index, item){
            $(item).css("pointer-events","none")
            $(item).attr("placeholder","");
        });

        $("select").each(function(index, item){
            $(item).css("pointer-events","none")
        });

        $("textarea").each(function(index, item){
            $(item).css("pointer-events","none")
            $(item).css("border-width","0")
        });

        $(".RemoveItems").css("display","none");
        $("#AddItems_Repeater_1").css("display","none");
        $("#DivButtons").css("display","none");
    }, 1000);
}

function ProcessProjectBreakDown(isIni){
    if(isIni){
        $("#Div_62 label").text("0");
        $("#Div_63 label").text("0");
        $("#Div_64 label").text("0");
        $("#Div_65 label").text("0");

        $("#Div_72 label").text("0");
        $("#Div_73 label").text("0");
        $("#Div_74 label").text("0");
        $("#Div_75 label").text("0");

        $("#Div_68 label").text("0");
        $("#Div_69 label").text("0");
        $("#Div_70 label").text("0");
        $("#Div_71 label").text("0");
    }
    else{
        var OriginalBudget = 0;
        var InitialCEA = 0;
        var RevisedCEA = 0;
        var TotalCEA = 0;

        var PerOriginalBudget = 0;
        var PerInitialCEA = 0;
        var PerRevisedCEA = 0;
        var PerTotalCEA = 0;

        $("[id$='DOriginalBudget']").each(function(index, item){
            OriginalBudget = Number(OriginalBudget) + Number($(item).val());
        });

        $("[id$='InitialCEA']").each(function(index, item){
            InitialCEA = Number(InitialCEA) + Number($(item).val());
        });

        $("[id$='RevisedCEA']").each(function(index, item){
            RevisedCEA = Number(RevisedCEA) + Number($(item).val());
        });

        TotalCEA = OriginalBudget + InitialCEA + RevisedCEA;

        PerOriginalBudget = 0;
        PerInitialCEA = InitialCEA * 0.15;
        PerRevisedCEA = RevisedCEA * 0.15;
        PerTotalCEA = TotalCEA * 0.15;

        $("#Div_62 label").text(OriginalBudget);
        $("#Div_63 label").text(InitialCEA);
        $("#Div_64 label").text(RevisedCEA);
        $("#Div_65 label").text(TotalCEA);

        $("#Div_72 label").text("-");
        $("#Div_73 label").text(PerInitialCEA.toFixed(2));
        $("#Div_74 label").text(PerRevisedCEA.toFixed(2));
        $("#Div_75 label").text(PerTotalCEA.toFixed(2));

        $("#Div_68 label").text(OriginalBudget);
        $("#Div_69 label").text(InitialCEA + Number(PerInitialCEA.toFixed(2)));
        $("#Div_70 label").text(RevisedCEA + Number(PerRevisedCEA.toFixed(2)));
        $("#Div_71 label").text(TotalCEA + Number(PerTotalCEA.toFixed(2)));
    }
}

function GetTotal(){
    $("[id$='DOriginalBudget']").each(function(index, item){
        $(item).on("keyup", function () {
            var thisID = $(this).attr("id").replace("DOriginalBudget", "");
            var OriginalBudget = $("#" + thisID + "DOriginalBudget").val();
            var InitialCEA = $("#" + thisID + "InitialCEA").val();
            var RevisedCEA = $("#" + thisID + "RevisedCEA").val();
            $("#" + thisID + "TotalCEA").val(Number(OriginalBudget) + Number(InitialCEA) + Number(RevisedCEA))
            ProcessProjectBreakDown(false);
        });
    });

    $("[id$='InitialCEA']").each(function(index, item){
        $(item).on("keyup", function () {
            var thisID = $(this).attr("id").replace("InitialCEA", "");
            var OriginalBudget = $("#" + thisID + "DOriginalBudget").val();
            var InitialCEA = $("#" + thisID + "InitialCEA").val();
            var RevisedCEA = $("#" + thisID + "RevisedCEA").val();
            $("#" + thisID + "TotalCEA").val(Number(OriginalBudget) + Number(InitialCEA) + Number(RevisedCEA))
            ProcessProjectBreakDown(false);
        });
    });

    $("[id$='RevisedCEA']").each(function(index, item){
        $(item).on("keyup", function () {
            var thisID = $(this).attr("id").replace("RevisedCEA", "");
            var OriginalBudget = $("#" + thisID + "DOriginalBudget").val();
            var InitialCEA = $("#" + thisID + "InitialCEA").val();
            var RevisedCEA = $("#" + thisID + "RevisedCEA").val();
            $("#" + thisID + "TotalCEA").val(Number(OriginalBudget) + Number(InitialCEA) + Number(RevisedCEA))
            ProcessProjectBreakDown(false);
        });
    });
}

function BindRoleInProjectTypeSettings(){
    $("[id$='RoleID']").each(function(index, item){
        $(item).on("change", function () {
            var thisID = $(this).attr("id").replace("RoleID", "RoleName");
            $("#" + thisID).val($(this).find('option:selected').text());
        });
    });
}

window.addRepeaterCallBack = function () {
    var currentView = CurrentViewStore.getCurrentView();

    switch (currentView) {
        case 'ApprovalSettings':

            BindRoleInProjectTypeSettings();

            break;

        case 'ProjectTypeSetting':

            $("[id$='DPropertyID']").each(function(index, item){
                $(item).val($("#PropertyID").val());
            });

            $("[id$='DPropertyName']").each(function(index, item){
                $(item).val($("#PropertyName").val());
            });

            BindRoleInProjectTypeSettings();

            break;

        case 'PropertyWiseYearlyBudget':

            $("[id$='DPropertyID']").each(function(index, item){
                $(item).val($("#PropertyID").val());
            });

            $("[id$='DPropertyName']").each(function(index, item){
                $(item).val($("#PropertyName").val());
            });

            break;

        case 'NewProject':
            GetTotal();
            ProcessProjectBreakDown(false);
            break;
    }
}

callProjectNotificationHandler = function (DID) {
    var from=getSearchPairs().fromtype;
    from = from!="Notification" ? "init":from;
    var url = AppOthersConstant.API_PREMISE_DOMAIN + "/api/" + "CEMProjectApi" + "/CallProjectNotificationHandler?AppKey=" + CommonStore.getAppKey() + "&Key=" + CommonStore.getKey() + "&DID=" + DID + "&From=" + from;
    //var url = "http://localhost:7478" + "/api/" + "CEMProjectApi" + "/CallProjectNotificationHandler?AppKey=" + CommonStore.getAppKey() + "&Key=" + CommonStore.getKey() + "&DID=" + DID + "&From=";
    $.ajax({
        async: true,
        type: 'GET',
        url: url
    }).done(function (data) {
    }).fail(function (data) {
        if (data.status == 401)
            window.location.href = "401.html";
    });
}

window.saveCallBack = function (data) {
    hideLoader();
    var currentView = CurrentViewStore.getCurrentView();

    switch (currentView) {

        case 'PurchaseOrder':
            SnackBar.show({text: "Information has been saved Successfully.", pos: 'bottom-center', showActionButton: false});
            window.history.pushState(history.state, "", "#Projects/PurchaseOrderList");
            window.dispatchEvent(new Event('popstate'));
            break;

        case 'ApprovalSettings':
            /*SnackBar.show({text: "Information has been saved Successfully.", pos: 'bottom-center', showActionButton: false});
            window.history.pushState(history.state, "", "#Settings/ApprovalSettingsList");
            window.dispatchEvent(new Event('popstate'));*/
            break;

        case 'ProjectTypeSetting':
            SnackBar.show({text: "Information has been saved Successfully.", pos: 'bottom-center', showActionButton: false});
            window.history.pushState(history.state, "", "#Settings/ProjectTypeSettingList");
            window.dispatchEvent(new Event('popstate'));
            break;

        case 'PropertyWiseYearlyBudget':
            SnackBar.show({text: "Information has been saved Successfully.", pos: 'bottom-center', showActionButton: false});
            window.history.pushState(history.state, "", "#Settings/PropertyWiseYearlyBudgetList");
            window.dispatchEvent(new Event('popstate'));
            break;

        case 'NewProject':
            var DID = CurrentViewStore.getCurrentEditId();
            if(DID == ""){
                DID = $("#project option:selected").val();
            }
            if(DID == "0"){
                var JSData = JSON.parse(data);
                DID = JSData.CEM_V1_Project;
            }
            callProjectNotificationHandler(DID);
            SnackBar.show({text: "Information has been saved Successfully.", pos: 'bottom-center', showActionButton: false});
            window.history.pushState(history.state, "", "#Projects/ProjectList");
            window.dispatchEvent(new Event('popstate'));
            break;
    }
};

