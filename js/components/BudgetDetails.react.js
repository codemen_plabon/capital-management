/**
 * Created by Dipon on 18/07/2016.
 */
var React = require('react');
var Mui = require('material-ui');
var $ = require('jquery');
var FlatButton = Mui.FlatButton;
var TextField = Mui.TextField;
var Table = Mui.Table;
var TableHeader = Mui.TableHeader;
var TableRow = Mui.TableRow;
var TableHeaderColumn = Mui.TableHeaderColumn;
var TableBody = Mui.TableBody;
var TableRowColumn = Mui.TableRowColumn;
var Select = require('react-select');
var _= require('underscore');
var CommonAction = require('../actions/Common.action');
var CommonStore = require('../stores/Common.store');
var CommonUtils = require('../utils/CommonUtils');
var CurrentViewAction = require('../actions/CurrentView.action');
var BudgetApi = require('../utils/ServerApi/BudgetApi');
var SnackBar = require('node-snackbar');

var BudgetDetails = React.createClass({

    getInitialState: function(){
        return {
            loadingShow:true,
            Header: [],
            Data:[],
            PreviousData:[],
            ChangeRowIndex:[],
            BudgetNumber:'',
            BudgetTitle:'',
            PropertyID:'',
            PropertName:'',
            PropertyList:[]


        };
    },

    componentDidMount: function() {
        var obj = this;
        BudgetApi.getBudgetByID(this.props.Id, function (data) {
            obj.setState({
                loadingShow:false,
                Header: data.Header,
                Data: data.Data,
                BudgetNumber:data.BudgetNumber,
                BudgetTitle:data.BudgetTitle,
                PropertyID:data.PropertyID,
                PropertyName:data.PropertyName,
                PropertyList:data.PropertyList,
                PreviousData:JSON.parse(JSON.stringify(data.Data))
            });
        });
    },

   headerGenerate:function(){
       var headerData = this.state.Header.map(function (subitem, index) {
           return (
               <th>{subitem}</th>
           );
       });
       return headerData;
   },

    handleInputChange: function(key,e){

        switch (key){

            case 'BudgetNumber':
                this.setState({BudgetNumber: e.target.value})
                break;

            case 'BudgetTitle':
                this.setState({BudgetTitle: e.target.value})
                break;

            default:
                return true;
        };
    },

    changeHandeler:function(row,column,e) {

        var previousData = this.state.PreviousData;
        var changeIndex = this.state.ChangeRowIndex;
        var data = this.state.Data;
        data[row][column] = e.target.value;

        var tempIndex=changeIndex.indexOf(row);
        if(tempIndex<0)
            changeIndex.push(row);

        if (previousData[row][column] != e.target.value)
            e.target.style.backgroundColor = "Salmon"
        else
            e.target.style.backgroundColor = "";


        this.setState({Data: data,ChangeRowIndex:changeIndex});
    },

    getData:function() {

        var obj = this;
        var data = this.state.Data.map(function (subitem, index) {
            return (
                <BudgetTableRowWarpper  key={index} changeHandeler={obj.changeHandeler} data={subitem} rowIndex={index}/>
            );
        });

        return data;
    },


    saveData:function(){

        var obj =this;
        var changeIndex = this.state.ChangeRowIndex;
        var tempData = this.state.Data;
        var finalData = new Array();

        for(var i=0;i<changeIndex.length;i++) {
            finalData.push(tempData[changeIndex[i]]);
        }
        var header =JSON.stringify(this.state.Header);
        finalData=JSON.stringify(finalData);
        var data={
            from:this.props.from,
            header:header,
            changeData:finalData,
            budgetId:this.props.Id,
            budgetNumber:this.state.BudgetNumber,
            budgetTitle:this.state.BudgetTitle,
            propertyId:this.state.PropertyID,
            propertyName:this.state.PropertyName
        };

        BudgetApi.UpdateBudget(data,function(success){

            if(success=="true") {
                SnackBar.show({
                    text: "Data has been saved successfully.",
                    pos: 'bottom-center',
                    showActionButton: false
                });
                window.history.pushState(history.state, "", "#Notifications");
                window.dispatchEvent(new Event('popstate'));
            }
            else
                SnackBar.show({text: "Failed to save.", pos: 'bottom-center', showActionButton: false});
        });
    },

    selectionChange: function(key,e){
        this.setState({PropertyID:e.value,PropertyName:e.label});
    },

    render: function () {

        var thisObj = this;
        return(

            <div>
                <div className="tableBuildingOuterDiv">
                    <div className="lbl-title">
                        Budget Details
                    </div>
                    <div className="form-area">
                        <div > <p>Property</p></div>
                        <Select
                            className="LeftSided SelectFullWidth HundredWidth MarginLessDdl CusMarginLessDdl"
                            name="Custom"
                            value={this.state.PropertyID}
                            onChange={this.selectionChange.bind(null,"PropertyID")}
                            options={this.state.PropertyList} />

                        <div className="cusLbl"><p>Budget No</p></div>

                        <div>
                        <TextField
                            ref="BudgetNo"
                            name="Subject"
                            value={this.state.BudgetNumber}
                            fullWidth={true}
                            onChange={this.handleInputChange.bind(null,'BudgetNumber')} />
                            </div>

                        <div className="cusLbl"><p>BudgetTitle</p></div>
                        <TextField
                            ref="BudgetTitle"
                            name="BudgetTitle"
                            value={this.state.BudgetTitle}
                            fullWidth={true}
                            onChange={this.handleInputChange.bind(null,'BudgetTitle')} />
                    </div>
                    <div className="lbl-title-sub lblTitleSub">
                        Budget Details
                    </div>
                    <div className="BuildingArea">
                        <div>
                            <p style={this.state.loadingShow==true?{display:'', textAlign:'center'}:{display:'none'}}>Loading</p>
                            <table className="BuildingListTable">
                                <thead>
                                <tr>
                                    {thisObj.headerGenerate()}
                                </tr>
                                </thead>
                                <tbody>
                                {this.getData()}
                                </tbody>
                            </table>

                        </div>
                    </div>
                    <div className="saveButton">
                        <input type="button" value="Save" onClick={this.saveData}/>
                    </div>
                </div>
            </div>
        );
    }
});

var BudgetTableRowWarpper = React.createClass({

    render: function () {

        var obj =this;
        var data = this.props.data.map(function (subitem, index) {

            if(index!=0) {
                return (
                    <BudgetTableColumnWarpper key={index} changeHandeler={obj.props.changeHandeler}
                                              rowIndex={obj.props.rowIndex} columnIndex={index} data={subitem}/>
                );
            }
        });

        return (
        <tr>
            {data}
        </tr>
        )
    }

});


var BudgetTableColumnWarpper = React.createClass({

    render: function () {

        return (
            <td><input type="text" value={this.props.data}  onChange={this.props.changeHandeler.bind(null,this.props.rowIndex,this.props.columnIndex)}/></td>
        )

    }

});



module.exports = BudgetDetails;