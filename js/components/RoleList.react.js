/**
 * Created by Dipon on 18/07/2016.
 */
var React = require('react');
var Mui = require('material-ui');
var $ = require('jquery');
var FlatButton = Mui.FlatButton;
var TextField = Mui.TextField;
var Table = Mui.Table;
var TableHeader = Mui.TableHeader;
var TableRow = Mui.TableRow;
var TableHeaderColumn = Mui.TableHeaderColumn;
var TableBody = Mui.TableBody;
var TableRowColumn = Mui.TableRowColumn;
var Select = require('react-select');
var TableFooter = Mui.TableFooter;
var _= require('underscore');
var CommonAction = require('../actions/Common.action');
var Pager = require('../utils/Pager.react');
var CommonApi = require('../utils/ServerApi/CommonApi');
var CommonStore = require('../stores/Common.store');
var AppMessage = require('../constants/AppMessage');
var RoleApi = require('../utils/ServerApi/RoleApi');
var TranslateApi = require('../utils/Translate');
var CommonUtils = require('../utils/CommonUtils');
var CurrentViewAction = require('../actions/CurrentView.action');
var TopMenuStore =  require('../stores/TopMenu.store');
var RoleList = React.createClass({

    getInitialState: function(){
        return {
            list: [],
            current:0,
            counter:0
        };
    },

    componentDidMount: function() {
        RoleApi.getRole("", this.onChange);
    },


    componentWillUnmount: function(){


    },



    onChange: function(roleListData,appId){
        this.setState({
            list: roleListData
        });
    },



    _onEdit:function(id,e) {

       // CurrentViewAction.addItem("AddUserRole",id);
        //  routie('AddRole/'+id);
        var keyValuePair = CommonUtils.getSearchPairs();
        keyValuePair.id = id;

        window.history.pushState(history.state, "", "?" + CommonUtils.getSearchStr(keyValuePair) + "#Role_Permission/Role");
        window.dispatchEvent(new Event('popstate'));

    },





    handlePageClick:function(pageNumber,e){

        this.setState({current:pageNumber});

    },

    deleteRole: function(Id){

        /*var isChildExist = CommonApi.IsChildExist_ByParentRowBDid(Id,parent_module,child_module);
         var gaugeIndex = _.findIndex(GaugeStore.getList(), {
         ID: Id
         });
         if(isChildExist)
         {
         CommonStore.setCommonErrorMessage(AppMessage.CANNOT_DELETE_GAUGE_CONFIRM);
         CommonAction.setDialogStatus(true,"NoEquipmentMessage","0");
         }
         else
         {
         CommonStore.setDeleteProp(Id,parent_module,gaugeIndex);
         CommonAction.setDialogStatus(true,"DeleteConfirm","0");
         //PropertyApi.deleteProperty(Id,propertyIndex);
         }*/
    },



    managePermission:function(did,e){

        var keyValuePair = CommonUtils.getSearchPairs();
        keyValuePair.id = did;

        window.history.pushState(history.state, "", "?" + CommonUtils.getSearchStr(keyValuePair) + "#Role_Permission/PermissionMapping");
        window.dispatchEvent(new Event('popstate'));
            //routie('RolePermissionMapping/'+did);
        //CurrentViewAction.addItem("RolePermissionMapping",did);
    },

    render: function () {

        var thisObj = this;

        var roleList = this.state.list;
        roleList = _.sortBy(roleList,function (i) {
            if(typeof i.RoleName === 'undefined'){
                // your code here.
                return;
            }
            else {
                return i.RoleName.toLowerCase();
            }

        });
        var currentPage  = this.state.current;
        var pageCount = Math.ceil((roleList.length)/10);

        var tempRoleList  = roleList.slice(currentPage*10,((currentPage*10)+10));


        var roleListRows = tempRoleList.map(function (subitem, index) {
            return (

                <RoleTableWarpper AppId={thisObj.state.AppId} App={TopMenuStore.getAppName(subitem.AppId)}  managePermission={thisObj.managePermission}  editClick={thisObj._onEdit}  key={index}  data={subitem} index={index} deleteRole = {thisObj.deleteRole}/>
            );
        }.bind(this));



        var tableStyle = {
            fixedHeader: true,
            fixedFooter: false,
            stripedRows: false,
            showRowHover: false,
            selectable: false,
            multiSelectable: false,
            enableSelectAll: false,
            deselectOnClickaway: false,
            height: '600px'
        };

        var columnName = (this.state.AppId==""||  this.state.AppId==undefined)?"App":"";
        return(

            <div>



                <div className="tableOuterDiv" style={{marginTop:'20px'}}>
                    <div className="BuildingHeaderArea">
                        <div className="LeftTitle">
                            {TranslateApi.convertText('Role')}{" "} {TranslateApi.convertText('List')}
                        </div>
                    </div>
                    <div>
                        <table className="BuildingListTable">
                            <thead>
                            <tr>
                                <th>{TranslateApi.convertText('RoleName')}</th>
                                <th>{TranslateApi.convertText('Description')}</th>


                                <th style={{width:"8%"}}></th>
                            </tr>
                            </thead>
                            <tbody>
                            {roleListRows}
                            </tbody>
                        </table>
                    </div>
                    <Pager total={pageCount} current={currentPage}
                           visiblePages={pageCount<10?pageCount:10}
                           onPageChanged={this.handlePageClick}/>
                </div>
            </div>
        );
    }

});


var RoleTableWarpper = React.createClass({

    render: function () {



        if((this.props.index-1)%2==0){
            return <tr>

                <td>{this.props.data.RoleName}</td>
                <td>{this.props.data.Description}</td>

                <td className="XY2">
                    <div className="hint  hint--left" data-hint="Click to edit">
                        <img onClick={this.props.editClick.bind(null,this.props.data.Did)}
                             style={{cursor: "pointer",width:'23px',height:'23px'}} src="./content/Icons_Final/edit.png"></img>
                    </div>

                    <div className="hint hint--left" data-hint="ManagePermission">
                        <img onClick={this.props.managePermission.bind(null,this.props.data.Did)}
                             style={{cursor: "pointer",width:'23px',height:'23px'}} src="./content/icn-file-locked-128.png"></img>
                    </div>
                </td>
            </tr>
        }
        else{
            return <tr>
                <td>{this.props.data.RoleName}</td>
                <td>{this.props.data.Description}</td>

                <td className="XY2">
                    <div className="hint  hint--left" data-hint="Click to edit">
                        <img onClick={this.props.editClick.bind(null,this.props.data.Did)}
                             style={{cursor: "pointer",width:'23px',height:'23px'}} src="./content/Icons_Final/edit.png"></img>
                    </div>

                    <div className="hint hint--left" data-hint="Manage Permission">
                        <img onClick={this.props.managePermission.bind(null,this.props.data.Did)}
                             style={{cursor: "pointer",width:'23px',height:'23px'}} src="./content/icn-file-locked-128.png"></img>
                    </div>
                </td>

            </tr>
        }
    }

});


module.exports = RoleList;