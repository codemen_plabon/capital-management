/**
 * Created by Dipon on 18/07/2016.
 */
var React = require('react');
var Mui = require('material-ui');
var $ = require('jquery');
var FlatButton = Mui.FlatButton;
var TextField = Mui.TextField;
var Table = Mui.Table;
var TableHeader = Mui.TableHeader;
var TableRow = Mui.TableRow;
var TableHeaderColumn = Mui.TableHeaderColumn;
var TableBody = Mui.TableBody;
var TableRowColumn = Mui.TableRowColumn;
var Select = require('react-select');
var TableFooter = Mui.TableFooter;
var _= require('underscore');
var Pager = require('../utils/Pager.react');
var CommonApi = require('../utils/ServerApi/CommonApi');
var CommonUtils = require('../utils/CommonUtils');
var CurrentViewAction = require('../actions/CurrentView.action');
var TopMenuStore =  require('../stores/TopMenu.store');

var ApprovalProjectList = React.createClass({

    getInitialState: function(){
        return {
            list: [],
            current:0,
            totalData:0,
        };
    },

    componentDidMount: function() {
        var obj = this;
        var query = "[\"{ $match:{BudgetApprovalStatus:'Approved'}}\", \"{ $skip:0}\" , \"{ $limit:10 } \"]";
        CommonApi.getTotalDataCount("CEM_V1_Project", "[\"{ $match:{BudgetApprovalStatus:'true'}}\"]", function (totalCount) {
            CommonApi.GetDataFromAggregate("CEM_V1_Project", query, function (Data) {
                obj.setState({
                    list: Data,
                    totalData: totalCount,
                    current: 0
                })
            });
        });
    },

    _onEdit:function(id,e) {

        var keyValuePair = CommonUtils.getSearchPairs();
        keyValuePair.id = id;
        window.history.pushState(history.state, "", "?" + CommonUtils.getSearchStr(keyValuePair) + "#Projects/NewProject");
        window.dispatchEvent(new Event('popstate'));

    },

    handlePageClick:function(pageNumber,e){

        var skip = pageNumber*10;
        var obj=this;
        var query = "[\"{ $match:{BudgetApprovalStatus:'Approved'}}\", \"{ $skip:"+skip+"}\" , \"{ $limit:10 } \"]";
        CommonApi.GetDataFromAggregate("CEM_V1_Project", query, function (Data) {
            obj.setState({
                list: Data,
                current: pageNumber
            })
        });
    },


    render: function () {

        var thisObj = this;
        var currentPage  = this.state.current;
        var totalData = this.state.totalData;
        var pageCount = Math.ceil((totalData)/10);


        var budgetData = this.state.list.map(function (subitem, index) {
            return (

                <ProjectTableWarpper AppId={thisObj.state.AppId} App={TopMenuStore.getAppName(subitem.AppId)}  managePermission={thisObj.managePermission}  editClick={thisObj._onEdit}  key={index}  data={subitem} index={index} deleteRole = {thisObj.deleteRole}/>
            );
        }.bind(this));

        var columnName = (this.state.AppId==""||  this.state.AppId==undefined)?"App":"";
        return(

            <div>
                <div className="tableOuterDiv" style={{marginTop:'20px'}}>
                    <div className="BuildingHeaderArea">
                        <div className="LeftTitle">
                            Project List
                        </div>
                    </div>
                    <div>
                        <table className="BuildingListTable">
                            <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Property Name</th>
                                <th>Year</th>
                                <th>Committed</th>
                                <th>Budget Type</th>
                                <th>Budget Timing</th>
                                <th>Capital Category</th>
                                <th style={{width:"4%"}}></th>
                            </tr>
                            </thead>
                            <tbody>
                            {budgetData}
                            </tbody>
                        </table>
                    </div>
                    <Pager total={pageCount} current={currentPage}
                           visiblePages={pageCount<10?pageCount:10}
                           onPageChanged={this.handlePageClick}/>
                </div>
            </div>
        );
    }
});

var ProjectTableWarpper = React.createClass({

    render: function () {

        return <tr>
            <td>{this.props.data.ProjectName}</td>
            <td>{this.props.data.PropertyName}</td>
            <td>{this.props.data.ProjectYear}</td>
            <td>{this.props.data.Committed}</td>
            <td>{this.props.data.BudgetType}</td>
            <td>{this.props.data.BudgetTiming}</td>
            <td>{this.props.data.CapitalCategory}</td>
            <td className="XY2">
                <div className="hint  hint--left" data-hint="Click to edit">
                    <img onClick={this.props.editClick.bind(null,this.props.data.Did)}
                         style={{cursor: "pointer",width:'23px',height:'23px'}} src="./content/Icons_Final/edit.png"></img>
                </div>
            </td>
        </tr>
    }
});
module.exports = ApprovalProjectList;