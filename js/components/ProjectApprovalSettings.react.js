/**
 * Created by Suza on 8/2/2016.
 */

var React = require('react');
var SettingApi = require('../utils/ServerApi/SettingApi');

var ProjectApprovalSetting = React.createClass({

    getInitialState: function(){
        return{

        }
    },

    componentDidMount: function(){
        SettingApi.getApprovalSettingForm(this.props.id, this.getRequestData);
    },

    componentWillReceiveProps: function(propsValue) {

    },

    getRequestData: function(RequestInfo){

        var leaseFormObj = document.getElementById("request-form-content");
        leaseFormObj.innerHTML = RequestInfo;
        var scriptCollection = leaseFormObj.getElementsByTagName('script');
        if (!scriptCollection.length) return;

        var scriptData = scriptCollection[0].innerHTML;
        var newScript = document.createElement('script');
        newScript.innerHTML = scriptData;
        newScript.type = "text/javascript";
        document.body.appendChild(newScript);
    },
    render: function(){

        return(
            <div className="form-area">
                <form id="request-form-content"></form>
                <input id="TestButton" type="button" value="Submit" style={{display:'none'}}/>
            </div>
        )
    }
});



module.exports = ProjectApprovalSetting

