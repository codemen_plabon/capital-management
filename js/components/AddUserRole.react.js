/**
 * Created by Dipon on 18/07/2016.
 */
var React = require('react');
var Mui = require('material-ui');
var TextField = Mui.TextField;
var FlatButton = Mui.FlatButton;
var RadioButtonGroup = Mui.RadioButtonGroup;
var RadioButton = Mui.RadioButton;
var Select = require('react-select');

var AppMessage = require('../constants/AppMessage');
var SnackBar = require('node-snackbar');
//var CurrentViewAction = require('../actions/CurrentView.action');
var TranslateApi = require('../utils/Translate');

var companyList = [];
var CurrentViewStore = require('../stores/CurrentView.store');

var RoleApi = require('../utils/ServerApi/RoleApi');
var CurrentViewAction = require('../actions/CurrentView.action');
var TopMenuStore =  require('../stores/TopMenu.store');

var AddUserRole = React.createClass({

    getInitialState: function(){
        return{
            Did: '',
            RoleName: '',
            Description: ''
        }
    },

    componentDidMount: function() {

        var roleId = this.props.id;
        if (roleId != "" && roleId!=undefined) {
            RoleApi.getRole(roleId,this.loadUpdatedData);
        }
    },

    componentWillReceiveProps: function(propsValue) {
        if (propsValue.id == ""){
            this.clearUser();
        }
    },

    loadUpdatedData: function(updatedData,AppId){
        this.setState({
            Did: updatedData[0].Did,
            RoleName: updatedData[0].RoleName,
            Description: updatedData[0].Description,

        });
    },

    handleInputChange: function(key,e){

        switch (key){

            case 'RoleName':
                this.setState({RoleName: e.target.value})
                break;

            case 'Description':
                this.setState({Description: e.target.value})
                break;

            default:
                return true;
        };
    },

    isValidation:function() {
        var f = true;
        if(this.state.RoleName == ""){ this.setState({RoleNameErrorMessage:"Role Name Required"}); f = false; } else this.setState({RoleNameErrorMessage: ""});

        return f;
    },

    handelFormSubmit:function(e){

        if(this.isValidation()){

            var userData = {
                Did: this.state.Did,
                RoleName: this.state.RoleName,
                Description: this.state.Description,

            };
            RoleApi.saveRole(userData, this.handleSaveSuccess);

        }
    },

    handleSaveSuccess: function(saveInfo){
        this.clearUser();
        if(saveInfo.Status === "Success"){
            SnackBar.show({text: "Role has been saved successfully.", pos: 'bottom-center', showActionButton: false});
        }
        //routie('RoleList');
        
        window.history.pushState(history.state, "", "#Role_Permission/RoleList");
        window.dispatchEvent(new Event('popstate'));

       // CurrentViewAction.addItem("RoleList");
    },

    clearUser:function(e) {
        this.setState({
            Did: "",
            RoleName: "",
            Description: '',
            AppId:'',
            RoleNameErrorMessage: ""
        });

    },

    render: function(){


        return(
            <div style={{marginTop: '20px'}}>

                <div className="BuildingHeaderArea">
                    <label className="LeftTitle">{TranslateApi.convertText('Role')}</label>
                </div>

                <div className="FormContainer">


                    <TextField
                        ref="RoleName"
                        name="RoleName"
                        value={this.state.RoleName}
                        floatingLabelText= {TranslateApi.convertText('RoleName')}
                        errorText = {this.state.RoleNameErrorMessage}
                        fullWidth={true}
                        onChange={this.handleInputChange.bind(null,'RoleName')}
                    />

                    <TextField
                        ref="Description"
                        name="Description"
                        value={this.state.Description}
                        floatingLabelText= {TranslateApi.convertText('Description')}
                        fullWidth={true}
                        onChange={this.handleInputChange.bind(null,'Description')}
                    />

                    <FlatButton className="FlatBtn btn-border-green" ref="save" label="Save" type="button" onClick={this.handelFormSubmit} />
                    <FlatButton className="FlatBtn btn-border-green" ref="reset" label="Reset" type="button" onClick={this.clearUser} />

                </div>
            </div>
        )
    }

});

module.exports = AddUserRole;