var React = require('react');

var LoginStore = require("../stores/Common.store.js");

var userProfileKey = 'Show_User_Profile_00089';
var companyProfileKey = 'Show_Company_Profile_00088';

if (window.location.host.search('demo') > -1) {
    userProfileKey = 'Show_User_Profile_00087';
    companyProfileKey = 'Show_Company_Profile_00086';
}

var getNodeFromRef = function (ref) {
    var node = ref;
    if (!node.nodeName) {
        node = React.findDOMNode(node);
        if (!node.nodeName) {
            node = node.getDOMNode();
        }
    }
    return node;
}

var UserOutlet = React.createClass({

    getInitialState: function () {
        var userInfo = LoginStore.getLoginData();
        var Permission = userInfo.Permission || "[]";
        Permission = JSON.parse(Permission);

        return {
            userInfo: LoginStore.getLoginData(),
            showUserProfile: Permission.find(function (i) {return i.PermissionKey == userProfileKey}),
            showCompanyProfile: Permission.find(function (i) {return i.PermissionKey == companyProfileKey}),
            expandInfo: false,
            popUp: ""
        }
    },

    _outsideClickCheck: function (e) {
        if (this.state.expandInfo) {
            if (!getNodeFromRef(this.refs['container']).contains(e.target)) {
                this.setState({expandInfo: false});
            }
        }
    },

    _showInfoContainer: function (e) {
        if (!this.state.expandInfo) {
            this.setState({
                expandInfo: true
            })
        }
    },

    _showUserProfile: function () {
        var userInfo = this.state.userInfo;

        var origin = window.location.origin;
        if (origin.search('localhost') > -1) origin = "http://dev.premisehq.co";

        var Key = userInfo.Key || LoginStore.getKey();
        var url = origin + "/Home/UserProfile?AppKey=" + userInfo.AppKey + "&Key=" + Key + "&Did=" + userInfo.UserId;

        this.setState({
            popUp: url
        })
    },

    _showCompanyProfile: function () {
        var userInfo = this.state.userInfo;

        var origin = window.location.origin;
        if (origin.search('localhost') > -1) origin = "http://dev.premisehq.co";

        var Key = userInfo.Key || LoginStore.getKey();
        var url = origin + "/Home/CompanyProfile?AppKey=" + userInfo.AppKey + "&Key=" + Key ;

        this.setState({
            popUp: url
        })

        /*var prevElem = document.querySelector('#user-outlet-popup-container');
        if (prevElem) prevElem.remove();

        document.body.insertAdjacentHTML('beforeend', '<div id="user-outlet-popup-container" style="position:fixed;z-index:99999;top:0px;left:0px;width:100%;height:100%;border-radius:10px;background-color:rgba(0,0,0,.5)"><div id="user-outlet-popup" style="position: relative; display: block; width:1000px; max-width: 80%; max-height: 80%; margin-top: 25px; padding-top: 20px;"><div style="position: absolute; right: 10px; top: 10px; color: black;">×</div><iframe src="' + url +'"></iframe></div></div>')
        var curElem = document.querySelector('#user-outlet-popup');
        curElem.querySelector('iframe').addEventListener('load', function (e) {
            curElem.style.height = this.contentDocument.body.offsetHeight + 40 + "px";
        })*/
    },

    _closePopUp: function () {
        setTimeout(function () {
            this.setState({popUp: ""});
        }.bind(this), 20);
    }, 

    _onIframeLoad: function (e) {
        var iframe = e.target;
        iframe.style.height = iframe.contentDocument.body.offsetHeight + "px";
    },

    _onChange: function () {
        var userInfo = LoginStore.getLoginData();
        var Permission = userInfo.Permission || "[]";
        Permission = JSON.parse(Permission);

        this.setState({
            userInfo: userInfo,
            showUserProfile: Permission.find(function (i) {return i.PermissionKey == userProfileKey}),
            showCompanyProfile: Permission.find(function (i) {return i.PermissionKey == companyProfileKey}),
        })
    },

    componentDidMount: function () {
        window.addEventListener("click", this._outsideClickCheck);
        LoginStore.addChangeListener(this._onChange);
    },

    componentWillReceiveProps: function (nextProps) {

    },

    componentWillUnmount: function () {
        window.removeEventListener("click", this._outsideClickCheck);
        LoginStore.removeChangeListener(this._onChange);
    },

    render: function () {
        var self = this;
        var stateObj = this.state;
        var props = this.props;

        var userInfo = stateObj.userInfo;
        var userName = null;

        if (userInfo) {
            userName = userInfo.FirstName;
            if (!userName) {
                userName = userInfo.UserName;
            } else {
                userName = userName + " " + userInfo.LastName || "";
            }
        }

        var pointerStyle = {
            borderColor: 'transparent',
            borderBottomColor: '#fff',
            borderStyle: 'dashed dashed solid',
            borderWidth: '0 8.5px 8.5px',
            position: 'absolute',
            right: '6.5px',
            top: '-6px',
            zIndex: '1',
            height: '0',
            width: '0'
        }

        return (
            <div ref="container" style={{position: 'relative'}}>

                {stateObj.popUp ?
                    <div
                        style={{
                            position: 'fixed',
                            width: '100%',
                            height: '100%',
                            backgroundColor: 'rgba(0,0,0,.5)',
                            zIndex: '9999',
                            left: '0px',
                            top: '0px'
                        }}>
                        <div 
                            id="user-outlet-popup"
                            style={{
                                display: 'block',
                                margin: '15px auto 0px',
                                width: '1040px',
                                maxWidth: '80%',
                                maxHeight: 'calc(100% - 30px)',
                                overflow: 'auto',
                                top: '0px',
                                left: '0px',
                                backgroundColor: 'white',
                                borderRadius: '8px',
                                padding: '20px' // To hold the close button
                            }}>
                            <div onClick={this._closePopUp} style={{position: 'absolute', right: '30px', top: '10px', color: '#fff', fontSize: '30px', fontWeight: 'bold', cursor: 'pointer'}}>
                                &times;
                            </div>
                            <iframe src={stateObj.popUp} onLoad={this._onIframeLoad} style={{width: '100%', border: 'none', minHeight: '300px'}} />
                        </div>
                    </div> : null}

                <a onClick={this._showInfoContainer} className="pull-right" href="javascript: void(0);" style={{height: '50px'}}>
                    <div id="switch" style={{height: '50px', float: 'right'}}/>
                </a>
                {userInfo && stateObj.expandInfo ?
                    <div
                        style={{
                            position: 'absolute',
                            width: '300px',
                            right: '5px',
                            boxShadow: '0px 1px 10px 0px rgba(0,0,0,.2)',
                            top: '45px',
                            padding: '12px 6px 6px',
                            backgroundColor: 'white',
                            zIndex: '21'
                        }}
                        >
                        <div style={pointerStyle}></div>
                        <div style={{display: 'table', borderBottom: '1px solid #bbb'}}>
                            <div style={{float: 'left', width: '105px'}}>
                                <img src="./content/css/images/default_person.jpg" />
                            </div>

                            <div style={{float: 'left', width: '183px', paddingLeft: '8px'}}>
                                <div>{userName}</div>
                                <div style={{fontSize: '12px'}}>{userInfo.Email}</div>
                                {stateObj.showUserProfile ?
                                    <div style={{margin: '6px 0px'}}>
                                        <button className="btn btn-xs btn-primary" onClick={this._showUserProfile}>User Profile</button>
                                    </div> : null}
                                {stateObj.showCompanyProfile ?
                                    <div style={{margin: '6px 0px'}}>
                                        <button className="btn btn-xs btn-default" onClick={this._showCompanyProfile}>Company Profile</button>
                                    </div> : null}
                            </div>

                        </div>
                        <div style={{display: 'inline-block', width: '100%', paddingTop: '9px'}}>
                            <a href="/Home/Login" style={{color: 'black', textDecoration: 'none'}}>
                                <button className="btn btn-sm btn-default pull-right">Sign Out</button>
                            </a>
                        </div>
                    </div> : null
                }
            </div>
        );
    }
});

module.exports = UserOutlet;