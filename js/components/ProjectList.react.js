/**
 * Created by Dipon on 18/07/2016.
 */
var React = require('react');
var Mui = require('material-ui');
var $ = require('jquery');
var FlatButton = Mui.FlatButton;
var TextField = Mui.TextField;
var Table = Mui.Table;
var TableHeader = Mui.TableHeader;
var TableRow = Mui.TableRow;
var TableHeaderColumn = Mui.TableHeaderColumn;
var TableBody = Mui.TableBody;
var TableRowColumn = Mui.TableRowColumn;
var Select = require('react-select');
var TableFooter = Mui.TableFooter;
var _= require('underscore');
var Pager = require('../utils/Pager.react');
var CommonApi = require('../utils/ServerApi/CommonApi');
var CommonUtils = require('../utils/CommonUtils');
var CurrentViewAction = require('../actions/CurrentView.action');
var TopMenuStore =  require('../stores/TopMenu.store');
var AppOthersConstant = require('../constants/AppOthersConstant');
var CommonStore = require('../stores/Common.store');
var ProjectList = React.createClass({

    getInitialState: function(){
        return {
            list: [],
            current:0,
            totalData:0,
            propertyList:[],
            property: '',
            project:''
        };
    },

    componentDidMount: function() {
        var obj = this;
        var query = "[\"{ $match:{ProjectYear:'2017'}}\", \"{ $skip:0}\" , \"{ $limit:10 } \"]";
        this.getPropertyData(function (property) {
            CommonApi.getTotalDataCount("CEM_V1_Project", "[\"{ $match:{ProjectYear:'2017'}}\"]", function (totalCount) {
                CommonApi.GetDataFromAggregate("CEM_V1_Project", query, function (Data) {
                    obj.setState({
                        list: Data,
                        totalData: totalCount,
                        current: 0,
                        propertyList:property
                    })
                });
            });
        });
    },

    _onEdit:function(id,e) {
        var keyValuePair = CommonUtils.getSearchPairs();
        keyValuePair.id = id;
        keyValuePair.fromtype = "ini";
        window.history.pushState(history.state, "", "?" + CommonUtils.getSearchStr(keyValuePair) + "#Projects/NewProject");
        window.dispatchEvent(new Event('popstate'));
    },

    goReadLonlyMode:function(id,e) {

        var keyValuePair = CommonUtils.getSearchPairs();
        keyValuePair.id = id;
        keyValuePair.fromtype="ini";
        keyValuePair.readOnly = "true";
        window.history.pushState(history.state, "", "?" + CommonUtils.getSearchStr(keyValuePair) + "#Projects/NewProject");
        window.dispatchEvent(new Event('popstate'));

    },

    handlePageClick:function(pageNumber,e){

        var temp="{ $match:{ProjectYear:'2017'";
        var propertyQuery = "";
        if(this.state.property!="" && this.state.property!=null){
            propertyQuery= ",PropertyID:'"+this.state.property+"'";
        }
        var projectQuery="";
        if(this.state.project!="" && this.state.project!=null){
            projectQuery= ",ProjectID:/"+this.state.property+"/";
        }
        if(propertyQuery!="" && projectQuery !=""){
            //temp=temp+propertyQuery+","+projectQuery+"}}";
        }
        else {
            //temp=temp+propertyQuery+projectQuery+"}}";
        }
        temp=temp+propertyQuery+projectQuery+"}}";
      //  var query = "["+temp+" ,\"{ $skip:0}\" , \"{ $limit:10 } \"]";
        var skip = pageNumber*10;
        var obj=this;
        var query = "[\""+temp+"\", \"{ $skip:"+skip+"}\" , \"{ $limit:10 } \"]";
        CommonApi.GetDataFromAggregate("CEM_V1_Project", query, function (Data) {
            obj.setState({
                list: Data,
                current: pageNumber
            })
        });
    },

    getPropertyData: function (callBack) {
        var thisObj = this;
        var url = AppOthersConstant.API_MAIN_DOMAIN + "/api/" + CommonStore.getAppKey() + "/ModuleData/Inventory_Building/SqlQuery/?key=" + CommonStore.getKey() + "&query={\"query\":[{ \"Where\": \"$and:[{ModuleKey} ]\"}]}";
        var option = [];
        $.ajax({
            async: true,
            type: 'GET',
            url: url
        }).done(function (data) {
            iniData = {
                value: "",
                label: "Select a Property"
            };
            option.push(iniData);
            var formatData = JSON.parse(data.Data);
            $.each(formatData, function (index, formatData) {
                data = {
                    value: formatData.Did,
                    label: formatData.BuildingName
                };
                option.push(data);
            });

            if(callBack)
                callBack(option);

        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
        });
    },

    onChangeProperty:function(event){
        thisObj = this;
        var propertyID = event.target.value;
        thisObj.setState({property:propertyID});
    },

    onTextBoxChange:function(e){
        this.setState({project:e.target.value});
    },

    searchHandeler:function(){
        var obj = this;
        var temp="{ $match:{ProjectYear:'2017'";
        var propertyQuery = "";
        if(this.state.property!="" && this.state.property!=null){
            propertyQuery= ",PropertyID:'"+this.state.property+"'";
        }
        var projectQuery="";
        if(this.state.project!="" && this.state.project!=null){
            projectQuery= ",ProjectName:/"+this.state.project+"/";
        }
        if(propertyQuery!="" && projectQuery !=""){
            //temp=temp+propertyQuery+","+projectQuery+"}}";
        }
        else {
            //temp=temp+propertyQuery+projectQuery+"}}";
        }
        temp=temp+propertyQuery+projectQuery+"}}";
        var query = "[\""+temp+"\" ,\"{ $skip:0}\" , \"{ $limit:10 } \"]";

            CommonApi.getTotalDataCount("CEM_V1_Project", "[\""+temp+"\"]", function (totalCount) {
                CommonApi.GetDataFromAggregate("CEM_V1_Project", query, function (Data) {
                    obj.setState({
                        list: Data,
                        totalData: totalCount,
                        current: 0
                    })

            });
        });
    },

    render: function () {

        var thisObj = this;
        var currentPage  = this.state.current;
        var totalData = this.state.totalData;
        var pageCount = Math.ceil((totalData)/10);


        var budgetData = this.state.list.map(function (subitem, index) {
            return (

                <ProjectTableWarpper goReadLonlyMode={this.goReadLonlyMode} editClick={thisObj._onEdit}  key={index}  data={subitem} index={index}/>
            );
        }.bind(this));

        var columnName = (this.state.AppId==""||  this.state.AppId==undefined)?"App":"";
        return(

            <div>
                <div style={{marginTop:'20px'}}>

                    <div id="divProperty" className="ap-form">
                        <label>Property</label>
                        <select id="property" onChange={this.onChangeProperty} defaultValue={this.state.property}>
                            {
                                this.state.propertyList.map(function(item) {
                                    return (<option value={item.value} >{item.label}</option>);
                                })
                            }
                        </select>
                    </div>

                    <div id="divProperty" className="ap-form">
                        <label>Project </label>
                        <input type="text" value={this.state.project} onChange={this.onTextBoxChange} />
                    </div>

                    <input id="btnSave" style={{marginBottom:'10px', cursor:'pointer'}} type="button" value="Search" onClick ={this.searchHandeler} />
                    <div className="BuildingHeaderArea">
                        <div className="LeftTitle">
                            Project List
                        </div>
                    </div>
                    <div>
                        <table className="BuildingListTable">
                            <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Property Name</th>
                                <th>Year</th>
                                <th>Committed</th>
                                <th>Budget Type</th>
                                <th>Budget Timing</th>
                                <th>Capital Category</th>
                                <th>Status</th>

                                <th style={{width:"4%"}}></th>
                            </tr>
                            </thead>
                            <tbody>
                            {budgetData}
                            </tbody>
                        </table>
                    </div>
                    <Pager total={pageCount} current={currentPage}
                           visiblePages={pageCount<10?pageCount:10}
                           onPageChanged={this.handlePageClick}/>
                </div>
            </div>
        );
    }
});

var ProjectTableWarpper = React.createClass({

    render: function () {

        return <tr>
            <td style={{cursor: "pointer"}} onClick={this.props.goReadLonlyMode.bind(null,this.props.data.Did)}>{this.props.data.ProjectName}</td>
            <td>{this.props.data.PropertyName}</td>
            <td>{this.props.data.ProjectYear}</td>
            <td>{this.props.data.Committed}</td>
            <td>{this.props.data.BudgetType}</td>
            <td>{this.props.data.BudgetTiming}</td>
            <td>{this.props.data.CapitalCategory}</td>
            <td>{this.props.data.ApprovalStatus}</td>
            <td className="XY2">
                <div className="hint  hint--left" data-hint="Click to edit">
                    <img onClick={this.props.editClick.bind(null,this.props.data.Did)}
                         style={{cursor: "pointer",width:'23px',height:'23px'}} src="./content/Icons_Final/edit.png"></img>
                </div>
            </td>
        </tr>
    }

});


module.exports = ProjectList;