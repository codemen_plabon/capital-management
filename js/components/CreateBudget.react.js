/**
 * Created by Suza on 8/2/2016.
 */

var React = require('react');
var AppOthersConstant  =require('../constants/AppOthersConstant');
var CommonStore = require('../stores/Common.store');
var SnackBar = require('node-snackbar');
var CommonUtils = require('../utils/CommonUtils');
var CreateBudget = React.createClass({

    getInitialState: function(){
        return {
            file: '',
            fileName: ''
        }
    },

    componentDidMount: function(){

    },

    fileUpload:function(e) {

        try {
            var obj = this;
            var filePath = e.target.files[0];
            obj.setState({file: filePath,fileName:filePath.name});
        }catch(ex) {

        }
    },

    controlFileUploadButton: function(e){
        var div = $(e.target).parent(".form-area");
        var file = $(div).find("[type='file']");
        $(file).click();
    },

    submitBudget:function(){

        showLoader("", "Please wait", 0);
        var url =AppOthersConstant.API_PREMISE_DOMAIN + "/api/CapitalExpence/UploadBudget?AppKey="+CommonStore.getAppKey()+"&Key="+CommonStore.getKey();

        var formData = new FormData();
        formData.append("file", this.state.file);
        formData.append("ResponsiblePersonID ", CommonStore.getUserFullName());
        formData.append("ResponsiblePersonName", CommonStore.getUserId());
        $.ajax({
            async: true,
            type: 'POST',
            url: url,
            data: formData,
            contentType: false,
            processData: false
        }).done(function (returnData) {
            hideLoader();
            if(returnData) {
                SnackBar.show({
                    text: "Data has been saved successfully.",
                    pos: 'bottom-center',
                    showActionButton: false
                });
                window.history.pushState(history.state, "", "#Notifications");
                window.dispatchEvent(new Event('popstate'));
            }
            else{
                SnackBar.show({
                    text: "Failed to save",
                    pos: 'bottom-center',
                    showActionButton: false
                });
            }
        }).fail(function (returnData) {
            if (returnData.status == 401)
                window.location.href = "401.html";
            else {
                hideLoader();
            }
        });
    },

     render: function(){

        return(
            <div className="form-area budget">
                <h2 className="lbl-title">Upload Budget</h2>
                <input id="attachments" style={{width:"90px", display: 'none'}} type="file"  name="file" onChange={this.fileUpload}/>
                <input className="fileUploadButton" onClick={this.controlFileUploadButton} type="button" value="Choose File"/> {this.state.fileName}

                <div>
                <input type="button" value="Submit" onClick={this.submitBudget}/>
                </div>
            </div>
        )
    }
});



module.exports = CreateBudget

