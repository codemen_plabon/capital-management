/**
 * Created by plabo on 26/07/2017.
 */
var React = require('react');
var $ = require('jquery');
var CommonStore=require('../stores/Common.store');
var AppOthersConstant = require('../constants/AppOthersConstant');
var Project = require('./Project.react');
var CommonUtils = require('../utils/CommonUtils');
var ProjectDetails = React.createClass({
    getInitialState: function(){
        return{
            propertyList: [{value: '0', label: 'Loading.....'}],
            property: '',
            projectList: [{value: '0', label: 'Loading.....'}],
            project: '',
            OtherComponent:''
        }
    },

    componentDidMount: function(){
        this.getPropertyData();

        // $("#divProject").css("display", "none");
    },

    componentWillReceiveProps: function(propsValue) {

    },

    getPropertyData: function () {
        var thisObj = this;
        var url = AppOthersConstant.API_MAIN_DOMAIN + "/api/" + CommonStore.getAppKey() + "/ModuleData/Inventory_Building/SqlQuery/?key=" + CommonStore.getKey() + "&query={\"query\":[{ \"Where\": \"$and:[{ModuleKey} ]\"}]}";
        var option = [];
        $.ajax({
            async: true,
            type: 'GET',
            url: url
        }).done(function (data) {
            iniData = {
                value: "0",
                label: "Select a Property"
            };
            option.push(iniData);

            var formatData = JSON.parse(data.Data);
            $.each(formatData, function (index, formatData) {
                data = {
                    value: formatData.Did,
                    label: formatData.BuildingName
                };
                option.push(data);
            });

            thisObj.setState({ propertyList: option })

        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
        });

    },

    getProjectData: function (property, callBack) {
        var thisObj = this;
        var url = AppOthersConstant.API_MAIN_DOMAIN + "/api/" + CommonStore.getAppKey() + "/ModuleData/CEM_V1_Project/SqlQuery/?key=" + CommonStore.getKey() + "&query={\"query\":[{ \"Where\": \"$and:[{ModuleKey},{BudgetApprovalStatus:'Approved'},{PropertyID:'"+ property +"'} ]\"}]}";
        var option = [];
        $.ajax({
            async: true,
            type: 'GET',
            url: url
        }).done(function (data) {
            iniData = {
                value: "0",
                label: "Select a Project"
            };
            option.push(iniData);

            var formatData = JSON.parse(data.Data);
            $.each(formatData, function (index, formatData) {
                data = {
                    value: formatData.Did,
                    label: formatData.ProjectName
                };
                option.push(data);
            });

            if(callBack){
                callBack(option);
            }

        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
        });

    },

    onChangeProperty:function(event){
        thisObj = this;
        var propertyID = event.target.value;
        this.getProjectData(propertyID, function (option) {
            thisObj.setState({property:propertyID, projectList: option });
        });
    },

    onChangeProject:function(event){
        this.loadForm(event.target.value);
    },

    loadForm:function(id){

        var keyValuePair = CommonUtils.getSearchPairs();
        keyValuePair.id = id;
        keyValuePair.fromtype="ini";
        keyValuePair.readOnly = "true";
        this.setCookie("Token","[{\"Appkey\":\""+CommonStore.getAppKey()+"\",\"Key\":\""+CommonStore.getKey()+"\",\"Page\":\""+"cd33e3ef9339400f85ea859534cf9ad3"+"\",\"DID\":\""+id+"\"}]",2);
        var view=<Project id ={id}/>;
        this.setState({ project: id,OtherComponent:view })
    },

    setCookie:function(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    },

    render: function(){

        return(
            <div className="form-area">
                <div className="">
                    <label className="lbl-title">Project Details Report</label>
                </div>
                <div id="divProperty" className="ap-form">
                    <label>Property</label>
                    <select id="property" onChange={this.onChangeProperty} defaultValue={this.state.property}>
                        {
                            this.state.propertyList.map(function(item) {
                                return (<option value={item.value} >{item.label}</option>);
                            })
                        }
                    </select>
                </div>
                <div id="divProject" className="ap-form">
                    <label>Project</label>
                    <select id="project" onChange={this.onChangeProject} defaultValue={this.state.project}>
                        {
                            this.state.projectList.map(function(item) {
                                return (<option value={item.value} >{item.label}</option>);
                            })
                        }
                    </select>
                </div>
                <div id="projectForm">
                    {this.state.OtherComponent}
                </div>
            </div>
        )
    }
});

module.exports = ProjectDetails