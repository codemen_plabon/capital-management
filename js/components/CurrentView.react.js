/**
 * Created by Plabon Ghosh on 24/08/2015.
 *
 * */

var React;
React = require('react');
var CurrentViewStore = require('../stores/CurrentView.store');
var CommonStore = require('../stores/Common.store');

var ProjectApprovalSetting = require('./ProjectApprovalSettings.react');
var AddRole = require('./AddUserRole.react');
var RoleList = require('./RoleList.react');
var ProjectTypeSetting = require('./ProjectTypeSettings.react');
var ProjectWiseYealyBudget = require('./ProjectWiseYealyBudget.react');

var Project = require('./Project.react');
var ProjectTypeSettingList = require('./ProjectTypeSettingList');
var ApprovalSettingsList = require('./ProjectApprovalSettingList.react');
var ProjectWiseYearlyBudgetList = require('./ProjectWiseYearlyBudgetList.react');
var ProjectList = require('./ProjectList.react');
var CreateBudget = require('./CreateBudget.react');
var BudgetList = require('./BudgetList.react');
var BudgetDetails = require('./BudgetDetails.react');
var BudgetApprovalSettings = require('./BudgetApprovalSettings.react');
var PurchaseOrder = require('./PurchaseOrder.react');
var PurchaseOrderList = require('./PurchaseOrderList.react');
var Notification = require('./Notification.react');
var ApprovalProjectList = require('./ApprovalProjectList.react');
var CommonUtils = require('../utils/CommonUtils.js');
var getSearchPairs = CommonUtils.getSearchPairs;
var IniProject = require('./IniProject.react');
var ProjectDetails = require('./ProjectDetails.react');
var ProjectReport = require('./ProjectReport.react');
var BudgetReport = require('./BudgetReport.react');
var BudgetFYE = require('./BudgetFYE.react');
var BudgetActual = require('./BudgetActual.react');
var FYEActual = require('./FYEActual.react');
var ContingencyProperty = require('./ContingencyProperty.react');
var CommonUtils = require('../utils/CommonUtils');
var getSearchPairs = CommonUtils.getSearchPairs;
var CurrentView = React.createClass({
    getInitialState: function(){
        return {
           currentView:'',
            id:''
        };
    },

     readTextFile:function(file, callback) {
         var rawFile = new XMLHttpRequest();
         rawFile.overrideMimeType("application/json");
         rawFile.open("GET", file, true);
         rawFile.onreadystatechange = function () {
             if (rawFile.readyState === 4 && rawFile.status == "200") {
                 callback(rawFile.responseText);
             }
         }
         rawFile.send(null);
     },
getParameterByNameIndex:function(name) {
        name = name.replace(/[\[]/, "\\[").replace(/[\]]/, "\\]");
        var regex = new RegExp("[\\?&]" + name + "=([^&#]*)"), results = regex.exec(location.search);
        return results === null ? "" : decodeURIComponent(results[1].replace(/\+/g, " "));
    },

    componentDidMount: function() {
        var thisObj = this;
        CurrentViewStore.addChangeListener(this.onChange);
    },

    componentWillUnmount: function(){
        CurrentViewStore.removeChangeListener(this.onChange);
    },

    onChange: function() {
        this.setState({
            currentView: CurrentViewStore.getCurrentView(),
            id : CurrentViewStore.getCurrentEditId()
        });
    },
    setCookie:function(cname, cvalue, exdays) {
        var d = new Date();
        d.setTime(d.getTime() + (exdays*24*60*60*1000));
        var expires = "expires="+ d.toUTCString();
        document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
    },
    render: function () {
       // var translate = TranslateApi.getContent(CommonStore.getLanguage()).page;
        var view;
        switch(this.state.currentView) {


            case 'AddRole':
                view = <AddRole  id={this.state.id}/>
                break;

            case 'RoleList':
                view = <RoleList AppId={this.state.id}/>
                break;

            case 'ApprovalSettings':
                var id = (this.state.id=="" ||  this.state.id==undefined)?"":  this.state.id;
                this.setCookie("Token","[{\"Appkey\":\""+CommonStore.getAppKey()+"\",\"Key\":\""+CommonStore.getKey()+"\",\"Page\":\""+"3b6223f816fb4900a59f904afa4c4d49"+"\",\"DID\":\""+"6af71e8335f9499d983e511225b606ed"+"\"}]",2);
                view = <BudgetApprovalSettings id ={id}/>
                break;

            case 'ProjectTypeSetting':
                var id = (this.state.id=="" ||  this.state.id==undefined)?"":  this.state.id;
                this.setCookie("Token","[{\"Appkey\":\""+CommonStore.getAppKey()+"\",\"Key\":\""+CommonStore.getKey()+"\",\"Page\":\""+"beeab531297d41bebc89391fc5c801d2"+"\",\"DID\":\""+id+"\"}]",2);
                view = <ProjectTypeSetting id ={id}/>
                break;

            case 'PropertyWiseYearlyBudget':
                var id = (this.state.id=="" ||  this.state.id==undefined)?"":  this.state.id;
                this.setCookie("Token","[{\"Appkey\":\""+CommonStore.getAppKey()+"\",\"Key\":\""+CommonStore.getKey()+"\",\"Page\":\""+"286bc405976c48989f8c1f6c90a13d4d"+"\",\"DID\":\""+id+"\"}]",2);
                view = <ProjectWiseYealyBudget id ={id}/>
                break;

            case 'NewProject':
                //var id = (this.state.id=="" ||  this.state.id==undefined)?"":  this.state.id;
                //this.setCookie("Token","[{\"Appkey\":\""+CommonStore.getAppKey()+"\",\"Key\":\""+CommonStore.getKey()+"\",\"Page\":\""+"cd33e3ef9339400f85ea859534cf9ad3"+"\",\"DID\":\""+id+"\"}]",2);

                if(getSearchPairs().fromtype=="" || getSearchPairs().fromtype==null || getSearchPairs().fromtype==undefined)
                  view = <IniProject/>
                else{
                    var id = (this.state.id=="" ||  this.state.id==undefined)?"":  this.state.id;
                    this.setCookie("Token","[{\"Appkey\":\""+CommonStore.getAppKey()+"\",\"Key\":\""+CommonStore.getKey()+"\",\"Page\":\""+"cd33e3ef9339400f85ea859534cf9ad3"+"\",\"DID\":\""+id+"\"}]",2);
                    view = <Project id={id}/>
                }
                break;

            case 'ProjectTypeSettingList':
                     view = <ProjectTypeSettingList/>
                break;

            case 'ApprovalSettingsList':
                view = <ApprovalSettingsList/>
                break;

            case 'PropertyWiseYearlyBudgetList':
                view = <ProjectWiseYearlyBudgetList/>
                break;
            case 'ProjectList':
                view = <ProjectList/>
                break;

            case 'BudgetApproveProjectList':
                view = <ApprovalProjectList/>
                break;

            case 'CreateBudget':
                view = <CreateBudget/>
                break;

            case 'BudgetList':
                view = <BudgetList/>
                break;
            case 'BudgetDetails':
                var temp=getSearchPairs().from;
                temp = (temp==undefined||temp==null)?"":temp;
                view = <BudgetDetails Id ={this.state.id} from={temp}/>
                break;

            case 'PurchaseOrder':
                var id = (this.state.id=="" ||  this.state.id==undefined)?"":  this.state.id;
                this.setCookie("Token","[{\"Appkey\":\""+CommonStore.getAppKey()+"\",\"Key\":\""+CommonStore.getKey()+"\",\"Page\":\""+"3aa2045771124de08094cd97d8d2520c"+"\",\"DID\":\""+id+"\"}]",2);
                view = <PurchaseOrder id ={id}/>
                break;

            case 'PurchaseOrderList':
                view = <PurchaseOrderList/>
                break;

            case 'Notification':
                view = <Notification/>
                //view = <ProjectDetails />
                break;

            case 'ProjectDetailReport':
                view = <ProjectDetails/>
                break;

            case 'ProjectReport':
                view = <ProjectReport/>
                break;

            case 'BudgetReport':
                view = <BudgetReport/>
                break;

            case 'BudgetFYE':
                view = <BudgetFYE/>
                break;

            case 'BudgetActual':
                view = <BudgetActual/>
                break;

            case 'FYEActual':
                view = <FYEActual/>
                break;

            case 'ContingencyProperty':
                view = <ContingencyProperty/>
                break;
            default:
                view ="";
            
        }

        return (
            <div>
                {view}
            </div>
        );
    }
});

module.exports = CurrentView;