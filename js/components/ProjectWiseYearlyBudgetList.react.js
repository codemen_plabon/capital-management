/**
 * Created by Suza on 6/29/2016.
 */

var React = require('react');
var CommonUtils = require('../utils/CommonUtils');
var _ = require('underscore');
var CommonStore = require('../stores/Common.store');

window.test3 = function (data) {
    console.log(data);
    var keyValuePair = CommonUtils.getSearchPairs();
    keyValuePair.id = data.__Master_Did;

    window.history.pushState(history.state, "", "?" + CommonUtils.getSearchStr(keyValuePair) + "#Settings/PropertyWiseYearlyBudget");
    window.dispatchEvent(new Event('popstate'));
}
var ProjectWiseYearlyBudgetList = React.createClass({

    getInitialState: function(){
        return{
            AppKey:CommonStore.getAppKey(),
            Key:CommonStore.getKey(),
            url:"Report/Index.html?AppKey="+CommonStore.getAppKey()+"&Key="+CommonStore.getKey()+"&application=Asset Inventory&name=Report3&filter=0&exc_fields=0&row_editable=1&callback=test3"
        };
    },

    componentDidMount: function(){

    },

    componentWillReceiveProps: function(propsValue) {
        if (propsValue.id == ""){
            UserApi.getUser(null,this.loadUser);
        }
    },


    render: function () {

        return (
            <div>
                <iframe style={{width:"100%",height:'1070px'}} src={this.state.url}></iframe>
            </div>
        )
    }
});


module.exports = ProjectWiseYearlyBudgetList;