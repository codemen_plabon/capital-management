/**
 * Created by Suza on 8/2/2016.
 */

var React = require('react');
var Mui = require('material-ui');
var $ = require('jquery');
var FlatButton = Mui.FlatButton;
var TextField = Mui.TextField;
var Table = Mui.Table;
var TableHeader = Mui.TableHeader;
var TableRow = Mui.TableRow;
var TableHeaderColumn = Mui.TableHeaderColumn;
var TableBody = Mui.TableBody;
var TableRowColumn = Mui.TableRowColumn;
var Select = require('react-select');
var TableFooter = Mui.TableFooter;
var _= require('underscore');
var CommonAction = require('../actions/Common.action');
var Pager = require('../utils/Pager.react');
var CommonApi = require('../utils/ServerApi/CommonApi');
var CommonStore = require('../stores/Common.store');
var AppMessage = require('../constants/AppMessage');
var RoleApi = require('../utils/ServerApi/RoleApi');
var TranslateApi = require('../utils/Translate');
var CommonUtils = require('../utils/CommonUtils');
var CurrentViewAction = require('../actions/CurrentView.action');
var TopMenuStore =  require('../stores/TopMenu.store');
var SettingApi = require('../utils/ServerApi/SettingApi');

var PurchaseOrderList = React.createClass({

    getInitialState: function(){
        return {
            list: [],
            current:0,
            totalData:0,
            invoiceList:[]
        };
    },

    componentDidMount: function(){
        var obj = this;
        var query = "[ \"{ $skip:0}\" , \"{ $limit:10 } \"]";
        CommonApi.getTotalDataCount("CEM_V1_ProjectPurchageOrder", "{}", function (totalCount) {
            CommonApi.GetDataFromAggregate("CEM_V1_ProjectPurchageOrder", query, function (Data) {
                obj.getInvoiceAmount(function(InvData){
                    obj.setState({
                        list: Data,
                        totalData: totalCount,
                        current: 0,
                        invoiceList: InvData
                    });
                });
            });
        });
    },

    _onEdit:function(id,e) {

        // CurrentViewAction.addItem("AddUserRole",id);
        //  routie('AddRole/'+id);
        var keyValuePair = CommonUtils.getSearchPairs();
        keyValuePair.id = id;

        window.history.pushState(history.state, "", "?" + CommonUtils.getSearchStr(keyValuePair) + "#Projects/PurchaseOrder");
        window.dispatchEvent(new Event('popstate'));

    },

    handlePageClick:function(pageNumber,e){

        var skip = pageNumber*10;
        var obj=this;
        var query = "[ \"{ $skip:"+skip+"}\" , \"{ $limit:10 } \"]";
        CommonApi.GetDataFromAggregate("CEM_V1_ProjectPurchageOrder", query, function (Data) {
            obj.setState({
                list: Data,
                current: pageNumber
            })
        });
    },

    getInvoiceAmount:function(callBack){
        var obj=this;
        var query = "[\"{ $match:{}}\"]";
        CommonApi.GetDataFromAggregate("CEM_V1_ProjectPurchageOrderDetails", query, function (Data) {
            if(callBack){
                callBack(Data);
            }
        });
    },

    render: function () {

        var thisObj = this;
        var currentPage  = this.state.current;
        var totalData = this.state.totalData;
        var pageCount = Math.ceil((totalData)/10);


        var PurchaseOrderListData = this.state.list.map(function (subitem, index) {
            var amount = 0;
            this.state.invoiceList.map(function(invItem,invIndex){
               if(invItem.MID==subitem.Did){
                   amount = amount + Number(invItem.Amount);
               }
            });
            return (

                <PurchaseOrderTableWarpper Amount={amount} AppId={thisObj.state.AppId} App={TopMenuStore.getAppName(subitem.AppId)}  managePermission={thisObj.managePermission}  editClick={thisObj._onEdit}  key={index}  data={subitem} index={index} deleteRole = {thisObj.deleteRole}/>
            );
        }.bind(this));

        var columnName = (this.state.AppId==""||  this.state.AppId==undefined)?"App":"";
        return(

            <div>
                <div className="tableOuterDiv" style={{marginTop:'20px'}}>
                    <div className="BuildingHeaderArea">
                        <div className="LeftTitle">
                           Purchase Order List
                        </div>
                    </div>
                    <div>
                        <table className="BuildingListTable">
                            <thead>
                            <tr>
                                <th>Project Name</th>
                                <th>Purchase Order Date</th>
                                <th>Purchase Order No</th>
                                <th>Amount</th>

                                <th style={{width:"8%"}}></th>
                            </tr>
                            </thead>
                            <tbody>
                            {PurchaseOrderListData}
                            </tbody>
                        </table>
                    </div>
                    <Pager total={pageCount} current={currentPage}
                           visiblePages={pageCount<10?pageCount:10}
                           onPageChanged={this.handlePageClick}/>
                </div>
            </div>
        );
    }
});

var PurchaseOrderTableWarpper = React.createClass({

    render: function () {

            return <tr>

                <td>{this.props.data.ProjectName}</td>
                <td>{this.props.data.PODate}</td>
                <td>{this.props.data.PONo}</td>
                <td>{this.props.Amount}</td>

                <td className="XY2">
                    <div className="hint  hint--left" data-hint="Click to edit">
                        <img onClick={this.props.editClick.bind(null,this.props.data.Did)}
                             style={{cursor: "pointer",width:'23px',height:'23px'}} src="./content/Icons_Final/edit.png"></img>
                    </div>
                </td>
            </tr>
        }

});

module.exports = PurchaseOrderList;