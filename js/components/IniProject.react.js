var React = require('react');
var $ = require('jquery');
var CommonStore=require('../stores/Common.store');
var AppOthersConstant = require('../constants/AppOthersConstant');
var Project = require('./Project.react');

var IniProject = React.createClass({
    getInitialState: function(){
        return{
            projectList: [{value: '0', label: 'Loading.....'}],
            project: '',
            OtherComponent:'',
            CEAType:''
        }
    },

    componentDidMount: function(){
        this.getProjectData();

       // $("#divProject").css("display", "none");
    },

    componentWillReceiveProps: function(propsValue) {

    },

    getProjectData: function () {
        var thisObj = this;
        var url = AppOthersConstant.API_MAIN_DOMAIN + "/api/" + CommonStore.getAppKey() + "/ModuleData/CEM_V1_Project/SqlQuery/?key=" + CommonStore.getKey() + "&query={\"query\":[{ \"Where\": \"$and:[{ModuleKey},{BudgetApprovalStatus:'Approved'},{ProjectYear:'2017'} ]\"}]}";
        var option = [];
        $.ajax({
            async: true,
            type: 'GET',
            url: url
        }).done(function (data) {
            iniData = {
                value: "0",
                label: "Select a Project"
            };
            option.push(iniData);

            var formatData = JSON.parse(data.Data);
            $.each(formatData, function (index, formatData) {
                data = {
                    value: formatData.Did,
                    label: formatData.ProjectName + " - " + formatData.ProjectYear
                };
                option.push(data);
            });

            thisObj.setState({ projectList: option })

        }).fail(function (data) {
            if (data.status == 401)
                window.location.href = "401.html";
        });

    },

    onChangeCEAType:function(event) {
    if(event.target.value == "Budgeted"){
        this.setState({CEAType:event.target.value});
        //$("#divProject").css("display", "block");
    }
    else{
        //$("#divProject").css("display", "none");
        this.setState({CEAType:event.target.value});
        this.loadForm("");
    }
},

onChangeProject:function(event){
    this.loadForm(event.target.value);
},

loadForm:function(id){

    this.setCookie("Token","[{\"Appkey\":\""+CommonStore.getAppKey()+"\",\"Key\":\""+CommonStore.getKey()+"\",\"Page\":\""+"cd33e3ef9339400f85ea859534cf9ad3"+"\",\"DID\":\""+id+"\"}]",2);
    var view=<Project id ={id}/>;
    this.setState({ project: id,OtherComponent:view })
},

setCookie:function(cname, cvalue, exdays) {
    var d = new Date();
    d.setTime(d.getTime() + (exdays*24*60*60*1000));
    var expires = "expires="+ d.toUTCString();
    document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
},

render: function(){

    return(
        <div className="form-area">
        <div className="">
        <label className="lbl-title">Project Form</label>
    </div>
    <div className="ap-form cea-type-check">
        <label>CEA Type</label>
    <div onChange={this.onChangeCEAType}>
<input className="button-rach button-radio" type="radio" value="Budgeted" name="CEAType"/>
        <label>Budgeted</label>
        <input className="button-rach button-radio" type="radio" value="Unbudgeted" name="CEAType"/>
        <label>Unbudgeted</label>
        <input className="button-rach button-radio" type="radio" value="Emergency" name="CEAType"/>
        <label>Emergency</label>
        </div>
        </div>
        <div style={this.state.CEAType!="Budgeted"?{display:'none'}:{display:''}} id="divProject" className="ap-form">
        <label>Project</label>
        <select id="project" onChange={this.onChangeProject} defaultValue={this.state.project}>
    {
        this.state.projectList.map(function(item) {
            return (<option value={item.value} >{item.label}</option>);
        })
    }
</select>
    </div>
    <div id="projectForm">
        {this.state.OtherComponent}
</div>
    </div>
)
}
});

module.exports = IniProject