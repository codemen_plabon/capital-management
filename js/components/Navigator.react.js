var React = require('react');

var CurrentViewAction = require('../actions/CurrentView.action');

//var Constant = require('../utils/Constant');

var NavMenuStore = require('../stores/TopMenu.store.js');
//var AppStateStore = require('../stores/AppStateStore');
//var SearchTemplateStore = require('../stores/SearchTemplateStore');
//var PropertyInfoStore = require('../stores/PropertyInfoStore');

var LoginStore = require('../stores/Common.store.js');

var UserOutlet = require('./UserOutlet');

var BreadCrumb = require('./General/BreadCrumb');
var NavBar = require('./General/NavBar');
//var NavHeaderRight = require('./notification/NavHeaderRight.react.js');

//var ServerAPI = require('../utils/ServerAPI');
var CommonUtils = require('../utils/CommonUtils.js');
var getSearchPairs = CommonUtils.getSearchPairs;

var breadCrumbData = [];

var prevHashItems = [];
var prevId = null;

var Navigator = React.createClass({

    getInitialState: function () {
        //var searchCredentials = AppStateStore.getSearchCredentials();
        //var searchCredentials = {};
        var searchCredentials = getSearchPairs();

        breadCrumbData = [{
            className: 'home-images rr-home',
            itemName: "",
            href: "/Home/Login"
        }, {
            itemName: "App",
            href: '/UserInfo/Apps/MyApps' + (searchCredentials.AppKey ? ('?AppKey=' + searchCredentials.AppKey + '&Key=' + searchCredentials.Key) : "")
        }, {
            itemName: "Capital Expense",
            href: '#'
        }]

        this.__initColor = null;

        return {
            //navMenuItems: NavMenuStore.getNavMenuItems(),
            //activeMenuArr: NavMenuStore.getActiveMenuArr()
        };
    },

    /*_onChange: function () {
        this.setState({
            navMenuItems: NavMenuStore.getNavMenuItems(),
            activeMenuArr: NavMenuStore.getActiveMenuArr()
        })
    },*/

    _styleChangeHandler: function () {
        var styleObj = LoginStore.getLoginData();
        if (!styleObj) return;

        var initColor = this.__initColor = {
            backgroundColor: '#fff', 
            borderBottomColor: '#fff', 
            hoveredItemBackgroundColor: '#fff',
            selectedItemBackgroundColor: '#fff', 
            selectedItemBorderBottomColor: '#fff',
            color: 'black',
            fontSize: '13px'
        };

        LoginStore.removeChangeListener(this._styleChangeHandler);

        styleObj = JSON.parse(styleObj.Style)[0];

        var mapping = {
            'Menu_Background_Color': 'backgroundColor',
            'Menu_Hover_Background_Color': 'hoveredItemBackgroundColor',
            'Menu_Selected_Background_Color': 'selectedItemBackgroundColor',
            'Menu_Color': 'color',
            'Menu_Font_Size': 'fontSize'            
        }

        Object.keys(mapping).map(function (key) {
            var c = styleObj[key];
            if (c) initColor[mapping[key]] = c;
        })

        var mapping_body = {
            'Font_Size': 'fontSize',
            'Font_Primary_Color': 'color'
        }

        var bodyStyle = document.body.style;
        Object.keys(mapping_body).map(function (key) {
            var c = styleObj[key];
            if (c) bodyStyle[mapping_body[key]] = c;
        })

        var mapping_other_gen = {
            'Background_Primary_Color': function (c) {
                return ".BuildingHeaderArea,.head-title{background-color:" + c + " !important}";
            },
            'Background_Secondary_Color': function (c) {
                return ".panel-body{background-color:" + c + "}";
            },
            'Font_Secondary_Color': function (c) {
                return ".footer{color:" + c + "}";
            }
        };

        var otherStyleHTML = "";
        Object.keys(mapping_other_gen).map(function (key) {
            var c = styleObj[key];
            if (c) otherStyleHTML += mapping_other_gen[key](c);
        })

        if (otherStyleHTML) {
            var otherStyleElem = document.createElement('style');
            otherStyleElem.id = 'customised-style';
            otherStyleElem.innerHTML = otherStyleHTML
            document.head.appendChild(otherStyleElem);
        }

        this.forceUpdate();
    },

    componentDidMount: function () {
        //NavMenuStore.addChangeListener(this._onChange);
        LoginStore.addChangeListener(this._styleChangeHandler);
        window.addEventListener('popstate', this._handleNavigation);
    },

    componentWillUnmount: function () {
        //NavMenuStore.removeChangeListener(this._onChange);
        LoginStore.removeChangeListener(this._styleChangeHandler);
        window.removeEventListener('popstate', this._handleNavigation);
    },

    _handleNavigation: function () {
        var self = this;
        var updateRequired = false;
        var hash = location.hash.substr(1) || "";
        var currentId = null;
        var hashItems = hash.split('/');

        var hashToActionMapper = {
            '': 'Notification',
            'Settings/ApprovalSettings': 'ApprovalSettings',
            'Settings/ApprovalSettingsList': 'ApprovalSettingsList',
            'Settings/ProjectTypeSetting': 'ProjectTypeSetting',
            'Settings/ProjectTypeSettingList':'ProjectTypeSettingList',
            'Settings/PropertyWiseYearlyBudget': 'PropertyWiseYearlyBudget',
            'Settings/PropertyWiseYearlyBudgetList': 'PropertyWiseYearlyBudgetList',
            'Projects/NewProject': 'NewProject',
            'Projects/ProjectList': 'ProjectList',
            'Budgets/NewBudget':'CreateBudget',
            'Budgets/BudgetList':'BudgetList',
            'BudgetDetails': 'BudgetDetails',
            'Projects/PurchaseOrder': 'PurchaseOrder',
            'Projects/PurchaseOrderList': 'PurchaseOrderList',
            'Projects/ApproveBudgetProject': 'BudgetApproveProjectList',
            'Notifications':'Notification',
            'Reports/ProjectDetailReport':'ProjectDetailReport',
            'Reports/ProjectsReport':'ProjectReport',
            'Reports/PropertyWiseBudget': 'BudgetReport',
            'Reports/BudgetFYE':'BudgetFYE',
            'Reports/BudgetActual':'BudgetActual',
            'Reports/FYEActual':'FYEActual',
            'Reports/ContingencyProperty':'ContingencyProperty'
        }

        /*if (hashItems.length == 1 && ['Tenant', 'User', 'Role_Permission', 'Settings'].indexOf(hashItems[0]) > -1) {
            var skipReplacing = false;
            if (hashItems[0] == 'Tenant') {
                hashItems[1] = "Company";
            } else if (hashItems[0] == 'User') {
                hashItems[1] = "User";
            } else if (hashItems[0] == 'Role_Permission') {
                hashItems[1] = "Role";
            } else if (hashItems[0] == 'Settings') {
                hashItems[1] = "HolidaySettings";
            } else {
                skipReplacing = true;
            }

            if (!skipReplacing) {
                hash = hashItems.join('/');
                window.history.replaceState(history.state, "", "#" + hash);
            }
        }*/

        if (hash in hashToActionMapper) {
            var newSearchPairs = getSearchPairs();
            currentId = newSearchPairs.id;
            if (prevHashItems.toString() != hashItems.toString() && prevId == currentId) {
                currentId = null;
                delete newSearchPairs.id;
                delete newSearchPairs.from;
                delete newSearchPairs.fromtype;
                delete newSearchPairs.readOnly;
                window.history.replaceState(history.state, "", "?" + CommonUtils.getSearchStr(newSearchPairs) + window.location.hash);
            }

            //if(hashToActionMapper[hash]=="ProjectDetailReport"){
            //    var keyValuePair = CommonUtils.getSearchPairs();
            //    keyValuePair.fromtype="ini";
            //    keyValuePair.readOnly = "true";
            //    var newSearchPairs = getSearchPairs();
            //    window.history.replaceState(history.state, "", "?" + CommonUtils.getSearchStr(newSearchPairs) + window.location.hash);
            //
            //}

            CurrentViewAction.addItem(hashToActionMapper[hash], currentId || "");
        }

        prevHashItems = hashItems;
        prevId = currentId;

        updateRequired = true;

        if (updateRequired || JSON.stringify(this.state.navMenuItems) != JSON.stringify(NavMenuStore.getNavMenuItems())) this.forceUpdate();
    },
   
    render: function() {
        var self = this;
        var stateObj = this.state;

        var ext_BCD = [].concat(breadCrumbData);

        var activeItemValue = [];

        var navMenuItems = NavMenuStore.getNavMenuItems();
        var activeMenuArr = NavMenuStore.getActiveMenuArr();

        if (activeMenuArr.length) {
            activeMenuArr.map(function (i) {
                ext_BCD.push({itemName: i.itemName, href: i.href});
                activeItemValue.push(i.value);
            })
        }

        if (ext_BCD.length > 3) {
            delete ext_BCD[ext_BCD.length - 1].href;
        }

        var initColor = this.__initColor;

        return (
            <div>   
                {initColor ? 
                    <NavBar
                        data={navMenuItems} 
                        elementRight={<UserOutlet />}
                        activeItemValue={activeItemValue}
                        initColor={initColor}
                    /> : null}
                {initColor ? <BreadCrumb data={ext_BCD} style={{fontWeight: 'bold', fontSize: initColor.fontSize || null}}/> : null}
            </div>
        );
    }
});


module.exports = Navigator;
