/**
 * Created by Suza on 8/2/2016.
 */

var React = require('react');
var Mui = require('material-ui');
var $ = require('jquery');
var FlatButton = Mui.FlatButton;
var TextField = Mui.TextField;
var Table = Mui.Table;
var TableHeader = Mui.TableHeader;
var TableRow = Mui.TableRow;
var TableHeaderColumn = Mui.TableHeaderColumn;
var TableBody = Mui.TableBody;
var TableRowColumn = Mui.TableRowColumn;
var Select = require('react-select');
var TableFooter = Mui.TableFooter;
var _= require('underscore');
var CommonAction = require('../actions/Common.action');
var Pager = require('../utils/Pager.react');
var CommonApi = require('../utils/ServerApi/CommonApi');
var CommonStore = require('../stores/Common.store');
var AppMessage = require('../constants/AppMessage');
var RoleApi = require('../utils/ServerApi/RoleApi');
var TranslateApi = require('../utils/Translate');
var CommonUtils = require('../utils/CommonUtils');
var CurrentViewAction = require('../actions/CurrentView.action');
var TopMenuStore =  require('../stores/TopMenu.store');
var SettingApi = require('../utils/ServerApi/SettingApi');

var ProjectTypeSettingList = React.createClass({

    getInitialState: function(){
        return {
            list: [],
            current:0,
            totalData:0,
        };
    },

    componentDidMount: function(){
        var obj = this;
        var query = "[ \"{ $skip:0}\" , \"{ $limit:10 } \"]";
        CommonApi.getTotalDataCount("CEM_V1_ProjectTypeSettings", "{}", function (totalCount) {
            CommonApi.GetDataFromAggregate("CEM_V1_ProjectTypeSettings", query, function (Data) {
                obj.setState({
                    list: Data,
                    totalData: totalCount,
                    current: 0
                })
            });
        });
    },

    _onEdit:function(id,e) {

        // CurrentViewAction.addItem("AddUserRole",id);
        //  routie('AddRole/'+id);
        var keyValuePair = CommonUtils.getSearchPairs();
        keyValuePair.id = id;

        window.history.pushState(history.state, "", "?" + CommonUtils.getSearchStr(keyValuePair) + "#Settings/ProjectTypeSetting");
        window.dispatchEvent(new Event('popstate'));

    },

    handlePageClick:function(pageNumber,e){

        var skip = pageNumber*10;
        var obj=this;
        var query = "[ \"{ $skip:"+skip+"}\" , \"{ $limit:10 } \"]";
        CommonApi.GetDataFromAggregate("CEM_V1_ProjectTypeSettings", query, function (Data) {
            obj.setState({
                list: Data,
                current: pageNumber
            })
        });
    },


    render: function () {

        var thisObj = this;
        var currentPage  = this.state.current;
        var totalData = this.state.totalData;
        var pageCount = Math.ceil((totalData)/10);


        var projectTypeSettingsData = this.state.list.map(function (subitem, index) {
            return (

                <ProjectTypeSettingsTableWarpper AppId={thisObj.state.AppId} App={TopMenuStore.getAppName(subitem.AppId)}  managePermission={thisObj.managePermission}  editClick={thisObj._onEdit}  key={index}  data={subitem} index={index} deleteRole = {thisObj.deleteRole}/>
            );
        }.bind(this));

        var columnName = (this.state.AppId==""||  this.state.AppId==undefined)?"App":"";
        return(

            <div>
                <div className="tableOuterDiv" style={{marginTop:'20px'}}>
                    <div className="BuildingHeaderArea">
                        <div className="LeftTitle">
                           Project Type Settings List
                        </div>
                    </div>
                    <div>
                        <table className="BuildingListTable">
                            <thead>
                            <tr>
                                <th>Property Name</th>
                                <th>Project Type</th>
                                <th>Standard</th>

                                <th style={{width:"8%"}}></th>
                            </tr>
                            </thead>
                            <tbody>
                            {projectTypeSettingsData}
                            </tbody>
                        </table>
                    </div>
                    <Pager total={pageCount} current={currentPage}
                           visiblePages={pageCount<10?pageCount:10}
                           onPageChanged={this.handlePageClick}/>
                </div>
            </div>
        );
    }
});

var ProjectTypeSettingsTableWarpper = React.createClass({

    render: function () {

            return <tr>

                <td>{this.props.data.PropertyName}</td>
                <td>{this.props.data.ProjectType}</td>
                <td>{this.props.data.ASTMStandardName}</td>

                <td className="XY2">
                    <div className="hint  hint--left" data-hint="Click to edit">
                        <img onClick={this.props.editClick.bind(null,this.props.data.Did)}
                             style={{cursor: "pointer",width:'23px',height:'23px'}} src="./content/Icons_Final/edit.png"></img>
                    </div>
                </td>
            </tr>
        }

});

module.exports = ProjectTypeSettingList;