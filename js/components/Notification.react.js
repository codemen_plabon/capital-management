/**
 * Created by Dipon on 18/07/2016.
 */
var React = require('react');
var Mui = require('material-ui');
var $ = require('jquery');
var FlatButton = Mui.FlatButton;
var TextField = Mui.TextField;
var Table = Mui.Table;
var TableHeader = Mui.TableHeader;
var TableRow = Mui.TableRow;
var TableHeaderColumn = Mui.TableHeaderColumn;
var TableBody = Mui.TableBody;
var TableRowColumn = Mui.TableRowColumn;
var Select = require('react-select');
var TableFooter = Mui.TableFooter;
var _= require('underscore');
var CommonAction = require('../actions/Common.action');
var Pager = require('../utils/Pager.react');
var CommonApi = require('../utils/ServerApi/CommonApi');
var CommonStore = require('../stores/Common.store');
var AppMessage = require('../constants/AppMessage');
var RoleApi = require('../utils/ServerApi/RoleApi');
var TranslateApi = require('../utils/Translate');
var CommonUtils = require('../utils/CommonUtils');
var CurrentViewAction = require('../actions/CurrentView.action');
var TopMenuStore =  require('../stores/TopMenu.store');
var Notification = React.createClass({

    getInitialState: function(){
        return {
            list: [],
            current:0,
            totalData:0,
        };
    },

    componentDidMount: function() {
        var obj = this;
        var query = "[ \"{ $skip:0}\" , \"{ $limit:10 } \"]";
        CommonApi.getTotalDataCount("CEM_V1_BudgetApprovalNotification", "{}", function (budgetTotalCount) {
            CommonApi.getTotalDataCount("CEM_V1_ProjectApprovalNotification", "{}", function (projectTotalCount) {
                CommonApi.GetDataFromAggregate("CEM_V1_BudgetApprovalNotification", query, function (budgetData) {
                    CommonApi.GetDataFromAggregate("CEM_V1_ProjectApprovalNotification", query, function (projectData) {
                        var Data=[];
                        $(budgetData).each(function(index,item){
                            Data.push({
                                Did:item.BudgetID,
                                NotificationDate:new Date(item.NotificationDate),
                                PropertyName:item.PropertyName,
                                Title:item.BudgetTitle,
                                Date:item.BudgetDate,
                                AssignTo:item.ToName,
                                Status:item.ToStatus,
                                Type:'Budget'
                            })
                        });

                        $(projectData).each(function(index,item){
                            Data.push({
                                Did:item.ProjectID,
                                NotificationDate: new Date(item.NotificationDate),
                                PropertyName:item.PropertyName,
                                Title:item.ProjectName,
                                Date:item.ProjDate,
                                AssignTo:item.ToName,
                                Status:item.ToStatus,
                                Type:'Project'
                            })
                        });
                        Data =   _.sortBy(Data, 'NotificationDate');

                        obj.setState({
                            list: Data,
                            totalData: parseInt(budgetTotalCount)+parseInt(projectTotalCount),
                            current: 0
                        })
                    });
                });
            });
        });
    },


    handlePageClick:function(pageNumber,e) {

        var skip = pageNumber * 10;
        var obj = this;
        var query = "[ \"{ $skip:" + skip + "}\" , \"{ $limit:10 } \"]";
        CommonApi.GetDataFromAggregate("CEM_V1_BudgetApprovalNotification", query, function (budgetData) {
            CommonApi.GetDataFromAggregate("CEM_V1_ProjectApprovalNotification", query, function (projectData) {

                var Data = [];
                $(budgetData).each(function (index, item) {
                    Data.push({
                        Did: item.BudgetID,
                        NotificationDate: new Date(item.NotificationDate),
                        PropertyName: item.PropertyName,
                        Title: item.BudgetTitle,
                        Date: item.BudgetDate,
                        AssignTo: item.ToName,
                        Status: item.ToStatus,
                        Type: 'Budget'
                    })
                });

                $(projectData).each(function (index, item) {
                    Data.push({
                        Did: item.ProjectID,
                        NotificationDate: new Date(item.NotificationDate),
                        PropertyName: item.PropertyName,
                        Title: item.ProjectName,
                        Date: item.ProjDate,
                        AssignTo: item.ToName,
                        Status: item.ToStatus,
                        Type: 'Project'
                    })
                });
                Data = _.sortBy(Data, 'NotificationDate');
                obj.setState({
                    list: Data,
                    current: pageNumber
                })
            })
        });
    },

    rowClick:function(id,type,e){
        var keyValuePair = CommonUtils.getSearchPairs();
        keyValuePair.id = id;

        if(type=="Budget") {
            keyValuePair.from = "Notification";
            window.history.pushState(history.state, "", "?" + CommonUtils.getSearchStr(keyValuePair) + "#BudgetDetails");
            window.dispatchEvent(new Event('popstate'));
        }

        else{
            keyValuePair.fromtype = "Notification";
            window.history.pushState(history.state, "", "?" + CommonUtils.getSearchStr(keyValuePair) + "#Projects/NewProject");
            window.dispatchEvent(new Event('popstate'));
        }
    },


    render: function () {

        var thisObj = this;
        var currentPage  = this.state.current;
        var totalData = this.state.totalData;
        var pageCount = Math.ceil((totalData)/10);


        var budgetData = this.state.list.map(function (subitem, index) {
            return (

                <BudgetTableWarpper rowClick={this.rowClick}   key={index}  data={subitem} index={index}/>
            );
        }.bind(this));


        return(

            <div>
                <div className="tableOuterDiv" style={{marginTop:'20px'}}>
                    <div className="BuildingHeaderArea">
                        <div className="LeftTitle">
                           Notifications
                        </div>
                    </div>
                    <div>
                        <table className="BuildingListTable">
                            <thead>
                            <tr>
                                <th>Type</th>
                                <th>Notification Date</th>
                                <th>Property Name</th>
                                <th>Title</th>
                                <th>Date</th>
                                <th>Assign To</th>
                                <th>Status</th>


                            </tr>
                            </thead>
                            <tbody>
                            {budgetData}
                            </tbody>
                        </table>
                    </div>
                    <Pager total={pageCount} current={currentPage}
                           visiblePages={pageCount<10?pageCount:10}
                           onPageChanged={this.handlePageClick}/>
                </div>
            </div>
        );
    }
});

var BudgetTableWarpper = React.createClass({

    render: function () {

        return(
          <tr style={{cursor:'pointer !important'}} onClick={this.props.rowClick.bind(null,this.props.data.Did,this.props.data.Type)}>
              <td>{this.props.data.Type}</td>
            <td>{CommonUtils.ToLocalDate(this.props.data.NotificationDate,"MM/DD/YYYY HH:mm")}</td>
            <td>{this.props.data.PropertyName}</td>
            <td>{this.props.data.Title}</td>
            <td>{CommonUtils.ToLocalDate(this.props.data.Date,"MM/DD/YYYY")}</td>
            <td>{this.props.data.AssignTo}</td>
            <td>{this.props.data.Status}</td>

        </tr>
        );
    }

});


module.exports = Notification;