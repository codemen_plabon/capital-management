/**
 * Created by Plabon on 15/10/2015.
 */


var React = require('react');
var TopMenuItem = require('./components/TopMenu.react');
var BackLink = require('./components/BackLink.react');
var Index = React.createClass({

    render: function () {
        return(
            <div>
                <TopMenuItem/>
                <BackLink/>
            </div>
        )
    }

});
module.exports = Index;