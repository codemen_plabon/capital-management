/**
 * Created by plabon on 10/08/2015.
 */

var React = require('react');
var AppDispatcher = require('../dispatcher/AppDispatcher');
var ObjectAssign = require('react/lib/Object.assign');
var EventEmitter = require('events').EventEmitter;
var CommonStore=require('../stores/Common.store');
var AppConstants = require('../constants/AppConstants');
var $ = require('jquery');
var _ = require('underscore');

var CHANGE_EVENT = 'change';
var PermittedApp = [];
var appList = [];
var Store = [];

var AddMenu = function (data) {
    Store = data;
}

var AddMenuFromServer = function (data) {
    AddMenu(data);
}

var AddPermittedApp = function (permittedAppList) {
    var formatPermittedList = JSON.parse(permittedAppList);
    var data;

    $.each(formatPermittedList, function (index, appData) {
        data = {
            Name: appData.Name,
            Icon: appData.Icon,
            Url: appData.Url,
            AppId:appData.AppId
        };

        if(appData.Name != "Capital Expense" && appData.Name != "UE") {
            PermittedApp.push(data);
        };

        appList.push(data);
    })
}

var searchCredentials = {};
var userInfo = {};

var activeMenuItem = "";
var activeMenuArr = [];

if (!location.hash.substr(1).split('/')[0]) {
    //history.replaceState(history.state, "", '#Notifications');
}

var allowedHash = {
    'BudgetDetails': true
}

var getItemSearch = function () {
    var subItemSearch = "";
    if (searchCredentials.AppKey) {
        subItemSearch = "?AppKey=" + searchCredentials.AppKey + "&Key=" + searchCredentials.Key;
    }
    return subItemSearch;
}

var setNavMenuApps = function (data) {
    var parser = 0, len = data.length;
    var subItems = navMenuItems['Capital Expense'].list;
    subItemSearch = getItemSearch();
    
    while (parser++ < len) {
        var item = data[parser - 1]; 
        var appName = item.Name;
        if (appName == "Capital Expense") {
            navMenuItems['Capital Expense'].img.src = item.Icon;
            continue;
        }

        subItems.push({
            itemName: appName,
            id: "",
            img: {
                src: item.Icon,
                style: imageStyle
            },
            href: item.Url + subItemSearch,
            value: appName
        });
    }
}

var imageStyle = {
    float: 'left',
    width: '20px',
    margin: '8px 5px 0px 0px',
    height: '20px'
}

var navMenuItems = {
    "Capital Expense": {
        itemName: "Capital Expense",
        list: [],
        id: "",
        className: '',
        img: {
            src: null,
            style: imageStyle
        },
        value: "Capital Expense"
    }
};

//var ActionCreator = require('../actions/ActionCreator');

var getKeyValuePair = function () {
    var keyValuePair = {};

    var getParams = location.search.substr(1);
    getParams.split('&').map(function (item) {
        var splitterIndex = item.indexOf('=');
        var key = item.substr(0, splitterIndex);
        if (key) {
            keyValuePair[key] = item.substr(splitterIndex + 1);
        }
    });
    return keyValuePair;
}

var getNavMenuItems = function () {
    var specificNavMenuItem = Object.keys(navMenuItems).map(function (key) {return navMenuItems[key];});
    /*var keyValuePairs = getKeyValuePair();
    if (keyValuePairs.P) {
        var newList = specificNavMenuItem[4].list = [];
        specificNavMenuItem[4].subItems.map(function (item) {newList.push(item);});
    } else {
        specificNavMenuItem[4].list = []
    }*/
    return specificNavMenuItem;
}

var setActiveMenuArr = function () {
    activeMenuArr = [];
    var split_activeMenuItem = activeMenuItem.split('/');
    var activeMenuObj = navMenuItems[split_activeMenuItem[0]];
    if (!activeMenuObj) {
        var val = split_activeMenuItem[0];
        if (allowedHash[val]) {
            activeMenuArr.push({
                ownHrefVal: val,
                value: val
            })
        }
        return;
    } else {
        activeMenuArr.push(activeMenuObj);
        var len = split_activeMenuItem.length;
        var childObj = null;
        for (var parser = 1; parser < len; parser++) {
            childObj = activeMenuObj.list.find(function (item, index) {
                return item.ownHrefVal == split_activeMenuItem[parser];
            })
            if (!childObj) {
                return;
            } else {
                activeMenuObj = childObj;
                activeMenuArr.push(childObj);
            }
        }

        if (len == 1) {
            var firstListItem = activeMenuObj.list[0];
            if (firstListItem) {
                activeMenuArr.push(firstListItem);
                split_activeMenuItem.push(firstListItem.ownHrefVal);
                window.history.replaceState(history.state, "", "#" + split_activeMenuItem.join("/"));
            }
        }
    }
}

var setActiveMenuItem = function (data) {
    var splitItem = data.split('/');
    if (navMenuItems[splitItem[0]] || allowedHash[splitItem[0]]) {
        activeMenuItem = data;
    } else {
        activeMenuItem = "";
    }
    activeMenuItem = data;
    setActiveMenuArr();
}

var handlePopState = function () {
    var locationHash = decodeURIComponent(location.hash).substr(1)
    if (locationHash.search('Capital Expense')) setActiveMenuItem(locationHash);
}

window.addEventListener('popstate', handlePopState);
handlePopState();

var setNavMenuItems = function (data) {
    var extendedMenuObj = {};

    data.map(function (item) {
        var mn = item.MenuName;
        var usl = mn.replace(/[^a-zA-Z ]/g, '').replace(/\s+/g, "_"); // Url-suited Link
        var ml = item.MenuLink;
        extendedMenuObj[usl] = {
            itemName: mn,
            list: [],
            //id: "",
            //className: '',
            //img: null,
            href: "#" + usl,
            ownHrefVal: usl,
            value: usl,
            Menus: item.Menus
        }
    })


    /* Case Specific for User System */


    /* End of Case Specific */


    // Since we are not extending menu levels to more than two, avoiding recursion here

    for (var key in extendedMenuObj) {
        var item = extendedMenuObj[key];
        item.Menus.map(function (i) {
            var mn = i.MenuName;
            var ml = i.MenuLink;
            item.list.push({
                itemName: mn,
                list: [],
                //id: "",
                //className: '',
                //img: null,
                //href: ml,
                href: "#" + key + "/" + ml.substr(1),
                //ownHrefVal: ml.split('/')[1], 
                ownHrefVal: ml.substr(1), 
                value: ml
            })
        })
        delete item.Menus;
    }
    extendedMenuObj['BudgetDetails'] =  {
        itemName: 'Budget Details',
        href: '#BudgetDetails',
        containerClassName: 'MenuHide',
        ownHrefVal: 'BudgetDetails',
        value: '#BudgetDetails'
    }
    Object.assign(navMenuItems, extendedMenuObj);

};

var elementRight = null;

var setElementRight = function (data) {
    var userName = data.FirstName;
    if (!userName) {
        userName = data.UserName;
    } else {
        userName = userName + " " + data.LastName || "";
    }

    if (userName) { 
        elementRight = (
            <div style={{display: 'inline-block'}}>
                <span style={{color: 'white', margin: '0px 3px', fontSize: '15px', lineHeight: '50px'}}>{userName}</span>
                <a href={"/Home/Login"} style={{height: '50px', float: 'right'}}>
                    <div id="switch" style={{height: '50px', float: 'right'}}></div>
                </a>
            </div>
        );
    }
}

var TopMenuStore = ObjectAssign({}, EventEmitter.prototype, {
    addChangeListener: function (cb) {
        this.on(CHANGE_EVENT, cb);
    },
    
    removeChangeListener: function (cb) {
        this.removeListener(CHANGE_EVENT, cb);
    },

    getList: function () {
        return Store;
    },

    getPermittedApp: function () {
        return PermittedApp;
    },

    getNavMenuApps: function () {
        return _navmenuapps;
    },

    getNavMenuItems: getNavMenuItems,

    getActiveMenuItem: function () {
        return activeMenuItem;
    },
    
    getActiveMenuArr: function () {
        return activeMenuArr;
    },

    getRightElement: function () {
        return elementRight;
    },

    getAppListForDropDown:function () {
        var dropDown = [];
        var tempArray = appList;
        
        for (var i = 0; i < tempArray.length; i++) {
            dropDown.push({
                value: tempArray[i].AppId,
                label: tempArray[i].Name,
            });
        }
        return dropDown;
    },

    getAppName:function (AppId) {

        if (AppId == undefined || AppId == "") {
            return "";
        }

        var data = _.where(appList, {AppId: AppId});

        if(data.length>0) return data[0].Name;
        else return "";
    }
});

AppDispatcher.register(function(payload){
    var action = payload.action;
    switch(action.actionType){
        case AppConstants.ADD_PERMITTED_APP:
            AddPermittedApp(action.data);
            setNavMenuApps(JSON.parse(action.data));
            TopMenuStore.emit(CHANGE_EVENT);
            break;

        case AppConstants.ADD_MENU_ITEMS_FROM_SERVER:
            AddMenuFromServer(action.data);
            setNavMenuItems(action.data);
            TopMenuStore.emit(CHANGE_EVENT);
            break;

        case "SET_SEARCH_CREDENTIALS":
            searchCredentials = action.data;
            return;

        case "SET_USER_INFO":
            userInfo = action.data;
            setElementRight(userInfo);
            break;
        default:
            return true;
    }
});

module.exports = TopMenuStore;