/**
 * Created by plabo on 16/10/2015.
 */

var AppDispatcher = require('../dispatcher/AppDispatcher');
var AppConstants = require('../constants/AppConstants');
var ObjectAssign = require('react/lib/Object.assign');
var EventEmitter = require('events').EventEmitter;
var CHANGE_EVENT = 'change';
var currentView="";
var editId="";
var otherInfo="";

var setCurrentView = function(item){
    currentView = item.viewName;
    editId=item.editId;
};

var CurrentViewStore = ObjectAssign({}, EventEmitter.prototype, {

    addChangeListener: function(cb){
        this.on(CHANGE_EVENT, cb);
    },

    removeChangeListener: function(cb){
        this.removeListener(CHANGE_EVENT, cb);
    },

    getCurrentView: function(){
        return currentView;
    },

    getCurrentEditId: function(){
        return editId;
    }

});

AppDispatcher.register(function(payload){

    var action = payload.action;
    switch(action.actionType){

        case AppConstants.SET_CURRENT_VIEW:
            setCurrentView(action.data);
            CurrentViewStore.emit(CHANGE_EVENT);
            break;

        default:
            return true;
    }
});

module.exports = CurrentViewStore;