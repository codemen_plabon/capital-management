/**
 * Created by plabon on 10/08/2015.
 */

var AppDispatcher = require('../dispatcher/AppDispatcher');
var ObjectAssign = require('react/lib/Object.assign');
var EventEmitter = require('events').EventEmitter;
var CommonStore=require('../stores/Common.store');
var AppConstants = require('../constants/AppConstants');
var $=require('jquery');

var _ =require('underscore');

var CHANGE_EVENT = 'change';
var PermittedApp=[];
var appList=[];
var Store = [];



var AddMenu = function(data){
    Store = data;

};


var AddMenuFromServer = function(data){
    AddMenu(data);
};


var AddPermittedApp=function(permittedAppList)
{
    var formatPermittedList = JSON.parse(permittedAppList);
    var data;
    $.each(formatPermittedList, function (index, appData) {
        data = {
            Name: appData.Name,
            Icon: appData.Icon,
            Url: appData.Url,
            AppId:appData.AppId
        };

        if(appData.Name!= "User System" && appData.Name!= "UE"){
            PermittedApp.push(data);
        };

        appList.push(data);
    });
};

var TopMenuStore = ObjectAssign({}, EventEmitter.prototype, {
    addChangeListener: function(cb){
        this.on(CHANGE_EVENT, cb);
    },
    removeChangeListener: function(cb){
        this.removeListener(CHANGE_EVENT, cb);
    },
    getList: function(){
        return Store;
    },
    getPermittedApp:function(){
    return PermittedApp;
    },

    getAppListForDropDown:function () {
       
        var dropDowm = [];
        var objthis = this;
        var tempArray = appList;
        
        for (var i = 0; i < tempArray.length; i++) {
            dropDowm.push({
                value: tempArray[i].AppId,
                label: tempArray[i].Name,
            });
        }
        return dropDowm;
    },

    getAppName:function(AppId){
        if(AppId==undefined || AppId=="")
return "";
        var data = _.where(appList, {AppId: AppId});

        if(data.length>0)
       return data[0].Name;
        else
            return "";
    }
});

AppDispatcher.register(function(payload){
    var action = payload.action;
    switch(action.actionType){
        case AppConstants.ADD_PERMITTED_APP:
            AddPermittedApp(action.data);
            TopMenuStore.emit(CHANGE_EVENT);
            break;

        case AppConstants.ADD_MENU_ITEMS_FROM_SERVER:
            AddMenuFromServer(action.data);
            TopMenuStore.emit(CHANGE_EVENT);
        default:
            return true;
    }
});

module.exports = TopMenuStore;