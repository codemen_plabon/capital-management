/**
 * Created by plabon on 10/08/2015.
 *
 * store only common variable value.
 */

var AppDispatcher = require('../dispatcher/AppDispatcher');
var AppOthersConstant = require('../constants/AppOthersConstant');
var objectAssign = require('react/lib/Object.assign');
var AppConstants = require('../constants/AppConstants');
var EventEmitter = require('events').EventEmitter;
var _ = require('underscore');

var CHANGE_EVENT = 'change';

var loginData = null;

var AppKey = "";
var UserName = "";
var UserFullName = "";
var UserType = "";
var UserPermission = "";
var UserId = "";
var Key = "";
var UserEmail = "";

var Language = "en";




var SetApiUrl = function (data) {
    AppOthersConstant.setUrl(data.nosqlUrl,data.premiseUrl);
};

var SetAppKey = function (data) {
    AppKey = data;
};

var SetKey = function (data) {
    Key = data;
};

var SetUserName = function (data) {
    UserName = data;
};

var SetUserFullName = function (data) {
    UserFullName = data;
};

var SetUserPermission = function (data) {
    UserPermission = data;
};

var SetUserType = function (data) {
    UserType = data;
};

var SetUserId = function (data) {
    UserId = data;
};

var SetUserEmail = function (data) {
    UserEmail = data;
};


var SetLanguage = function (data) {
    Language = data;
};




var CommonStore = objectAssign({}, EventEmitter.prototype, {
    addChangeListener: function (cb) {
        this.on(CHANGE_EVENT, cb);
    },
    removeChangeListener: function (cb) {
        this.removeListener(CHANGE_EVENT, cb);
    },

    getIsSignUp: function () {
        return IsSignIp;
    },
    getAppKey: function () {
        return AppKey;
    },

    getKey: function () {
        return Key;
    },

    getUserName: function () {
        return UserName;
    },

    getUserFullName: function () {
        return UserFullName;
    },

    getUserId: function () {
        return UserId;
    },

    getUserPermission: function () {
        return UserPermission;
    },

    getUserType: function () {
        return UserType;
    },

    getUserEmail: function () {
        return UserEmail;
    },


    getLanguage: function () {
        return Language;
    },

    getLoginData: function () {
        return loginData;
    }

});

AppDispatcher.register(function (payload) {
    var action = payload.action;
    switch (action.actionType) {


        case AppConstants.ADD_APP_KEY:
            SetAppKey(action.data);
            CommonStore.emit(CHANGE_EVENT);
            break;

        case AppConstants.ADD_KEY:
            SetKey(action.data);
            CommonStore.emit(CHANGE_EVENT);
            break;

        case AppConstants.ADD_USER_NAME:
            SetUserName(action.data);
            CommonStore.emit(CHANGE_EVENT);
            break;

        case AppConstants.ADD_USER_FULL_NAME:
            SetUserFullName(action.data);
            CommonStore.emit(CHANGE_EVENT);
            break;

        case AppConstants.ADD_USER_ID:
            SetUserId(action.data);
            CommonStore.emit(CHANGE_EVENT);
            break;

        case AppConstants.ADD_USER_TYPE:
            SetUserType(action.data);
            CommonStore.emit(CHANGE_EVENT);
            break;

        case AppConstants.ADD_USER_PERMISSION:
            SetUserPermission(action.data);
            CommonStore.emit(CHANGE_EVENT);
            break;

        case AppConstants.ADD_USER_EMAIL:
            SetUserEmail(action.data);
            CommonStore.emit(CHANGE_EVENT);
            break;

        case AppConstants.SET_LANGUAGE:
            SetLanguage(action.data);
            CommonStore.emit(CHANGE_EVENT);
            break;

        case AppConstants.SET_API_URL:
            SetApiUrl(action.data);
            CommonStore.emit(CHANGE_EVENT);
            break;

        case "SET_USER_INFO":
            loginData = action.data;
            CommonStore.emit(CHANGE_EVENT);
            break;

        default:
            return true;
    }
});

module.exports = CommonStore;