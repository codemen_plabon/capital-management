
function showLoader(icon, message, overlayTop) {

    var htmlLoader = '';
    htmlLoader += '<div id="loaderOverlay" class="loaderOverlay">';
    htmlLoader += '<div class="dialog-loading-wrapper">';
    if (icon != "") {
        htmlLoader += '<div id="loaderIcon" style="background-image:url(' + icon + ')">';
        htmlLoader += '</div>';
    }
    htmlLoader += '<div id="circularG">';
    htmlLoader += '<div id="circularG_1" class="circularG"></div>';
    htmlLoader += '<div id="circularG_2" class="circularG"></div>';
    htmlLoader += '<div id="circularG_3" class="circularG"></div>';
    htmlLoader += '</div>';
    htmlLoader += '<div id="loaderMessage">';
    htmlLoader += message;
    htmlLoader += '</div>';
    htmlLoader += '</div>';
    htmlLoader += '</div>';

    $("body").append(htmlLoader);

    $(".loaderOverlay").css('top', overlayTop);
};

function hideLoader() {
    if ($("#loaderOverlay")) {
        $("#loaderOverlay").remove();
    }
};
